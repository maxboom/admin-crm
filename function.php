<?php
	function db_connect()
	{
		$host = 'localhost';
		$user = 'u_admincrm';
		$password = 'fQBUdL9v';
		$db = 'admincrm';
		
		$connect = mysql_connect($host,$user,$password);
		mysql_query("SET NAMES utf8");
		
		if(!$connect || !mysql_select_db($db,$connect)) {
			return false;
		}
		return $connect;
	}

	function sql_injection($variable){
		$variable = strip_tags(mysql_real_escape_string(trim($variable)));
		return $variable; 
	}
	


	function db_result_to_array($result) {
		
		db_connect();
		$result_array = array();
		$count = 0;

		while($row=mysql_fetch_assoc($result)) {
		$result_array[$count] = $row;
		$count++;
		}

		return $result_array;
	}

	function get_all_users() {
		db_connect();
		$SQL = mysql_query('SELECT * FROM `clients`');

		$result = db_result_to_array($SQL);
		return $result;
	}

	function get_all_orders() {
		db_connect();
		$SQL = mysql_query('SELECT * FROM `orders`');

		$result = db_result_to_array($SQL);
		return $result;
	}
    
    function quote($value)
    {
        return str_replace("'", "`", $value);
    } // end quote
    
    function prepareData(
        $data = array(), 
        $prepare = array(),
        &$error = null
    )
    {
        foreach ($prepare as $prepareKey => $prepareFilter)
        {
            if (!array_key_exists($prepareKey, $data))
            {
                $error = $prepareKey;
                return;
            }
            
            $predareValue = $data[$prepareKey];
            
            if (!filter_var($predareValue, $prepareFilter))
            {
                $error = $prepareFilter;
                return;
            }
        }
        
        return $data;
    } // end prepareData
?>