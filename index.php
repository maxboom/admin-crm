<?php
session_start();
include_once('function.php');
const ROOT = '/';
$pages_url = $_SERVER["REQUEST_URI"];
$alias = $pages_url;

if(strripos($alias, '?')) {
 $data = stristr($alias, '?');
 $data = explode('&', $data);

 foreach ($data as $value) {
  $value = explode('=', $value);
  $_GET[$value[0]] = $value[1];
 }

 $alias = stristr($alias, '?', true); 
}

$alias = checkAlias($alias);

if (is_array($alias)) 
{
	$id_device = $alias['id'];
	$alias = $alias['type'];
}

switch ($alias) 
{
	case 'index':	
		break;
	case 'orders':
		$orders = get_all_orders();
		break;
	case 'clients':
		$users = get_all_users();
		break;
	case 'calls':
		break;
	case 'asterisksms':
		break;
	case 'simcards':
		break;
	case 'goodsinstock':
		break;
	case 'goods':
		break;
	case 'delivery':
		break;
	case 'sent':
		break;
	case 'states':
		break;
	case 'cities':
		break;
	case 'zipcodes':
		break;
	case 'calling':
		break;
	case 'monitoring':
		break;
	case 'pack':
		break;
	case 'toate':
		break;
	case 'invoice':
		break;
	case 'users':
		break;
	case 'statistic':
		break;
	case 'operator_bonus':
		break;
	case 'packages_statistic':
		break;
	case 'admin':
		break;

}

function checkAlias($alias)
{
    $alias = mb_split("/", $alias);
    $page = $alias;

    if($page[1] === 'activate') {
        if (count($page[2]) > 0)
        {
            return $alias = array('type' => 'activate', 'id' => $page[2]);
        } 
        else 
        {
            return false;
        }
    }

    if ($page[1])
    {
        return  $alias = $page[1];
    }
    else
    {
        return $alias = 'index';
    }
}

if (is_array($alias)) 
{
	$page = $alias['type'];
}
else
{
	$page = $alias;
}

$arr = array('index','clients','orders','admin','calls','asterisksms','simcards','goodsinstock'
,'goods','delivery','sent','states','cities','zipcodes','calling','monitoring','pack','toate'
,'invoice','users','statistic','operator_bonus','packages_statistic');

include($_SERVER['DOCUMENT_ROOT'].'/views/layouts/index.php');

?> 