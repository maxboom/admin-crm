<?php
include('../../function.php');
db_connect();

$data = prepareData($_POST, array(
        'id' => FILTER_VALIDATE_INT
    ), $error
);

if ($error)
{
    throw new Exception ("Page not found! " . $error);
}

$id = $data['id'];

$sql = "SELECT * 
    FROM orders, clients 
    WHERE orders.id=" . quote($id);

$info = mysql_query($sql);

$infoArr = mysql_fetch_assoc($info);
echo json_encode($infoArr);
?>
