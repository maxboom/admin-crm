
<section class="content-header">
    <h1>
        Goods
        <small>Goods list</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Warehouse</li>
    </ol>
</section>

<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">
                    
                    <a href="/panel/goods/create/" class="btn btn-sm btn-primary">
                        <span class="glyphicon glyphicon-new-window"></span> <b>New </b>
                    </a>
                    
                </div>
                <div class="box-body table-responsive">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
								<th>ID</th>
								<th><input class="form-control" id="id_name" maxlength="128" name="name" type="text"></th>
								<th><input class="form-control" id="id_weight" name="weight" type="number"></th>

                                <th><input class="form-control" id="id_prime_price" name="prime_price" type="text"></th>
                                <th><select class="form-control select2 select2-hidden-accessible" id="id_prime_price_currency" name="prime_price_currency" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="2">Польский злотый</option>
<option value="3">Евро</option>
<option value="4">Румынский лей</option>
<option value="1">Украинская гривна</option>
<option value="5">Российский рубль</option>
<option value="6">Доллар США</option>
<option value="7">Венгерский форинт</option>
<option value="8">Словацкая крона</option>
<option value="9">Болгарский лев</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 236px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_prime_price_currency-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
								<th>ID</th>
								<th>Nume</th>
								<th>Weight</th>

                                <th>Prime price</th>
                                <th>Prime price currency</th>
                                <th style="width: 116px; min-width:116px;">
                                </th>
							</tr>
						</thead>
						<tbody>
						
							<tr>
								<td style="width: 40px;">59</td>
								<td>BigBust Firming Up</td>
								<td>80</td>

                                <td>3,0</td>
                                <td>Евро</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        
                                            <a href="/panel/goods/edit/59/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td style="width: 40px;">58</td>
								<td>BigBust Shape Up</td>
								<td>80</td>

                                <td>3,0</td>
                                <td>Евро</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        
                                            <a href="/panel/goods/edit/58/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td style="width: 40px;">57</td>
								<td>Fixline  Weight Control </td>
								<td>125</td>

                                <td>4,0</td>
                                <td>Евро</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        
                                            <a href="/panel/goods/edit/57/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td style="width: 40px;">56</td>
								<td>Fixline Detox </td>
								<td>250</td>

                                <td>4,0</td>
                                <td>Евро</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        
                                            <a href="/panel/goods/edit/56/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td style="width: 40px;">55</td>
								<td>Fungalor</td>
								<td>80</td>

                                <td>3,5</td>
                                <td>Доллар США</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        
                                            <a href="/panel/goods/edit/55/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td style="width: 40px;">54</td>
								<td>Thor's Hammer</td>
								<td>100</td>

                                <td>3,5</td>
                                <td>Доллар США</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        
                                            <a href="/panel/goods/edit/54/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td style="width: 40px;">53</td>
								<td>Toretto Necklace </td>
								<td>50</td>

                                <td>2,2</td>
                                <td>Доллар США</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        
                                            <a href="/panel/goods/edit/53/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td style="width: 40px;">52</td>
								<td>Heart of ocean</td>
								<td>50</td>

                                <td>2,2</td>
                                <td>Доллар США</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        
                                            <a href="/panel/goods/edit/52/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td style="width: 40px;">51</td>
								<td>Braslet Xpower </td>
								<td>25</td>

                                <td>2,2</td>
                                <td>Доллар США</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        
                                            <a href="/panel/goods/edit/51/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td style="width: 40px;">50</td>
								<td>Forskolin 2</td>
								<td>100</td>

                                <td>4,0</td>
                                <td>Доллар США</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        
                                            <a href="/panel/goods/edit/50/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        


                                    </div>
                                </td>
							</tr>
						
						</tbody>
					</table>

                    


<ul class="pagination">

  <li class="disabled"><a href="#" onclick="javascript: return false;">«</a></li>


  
    
  <li class="active"><a href="#" onclick="javascript: return false;">1</a></li>
    
  

  
    
  <li><a href="?page=2">2</a></li>
    
  

  
    
  <li><a href="?page=3">3</a></li>
    
  

  
    
  <li><a href="?page=4">4</a></li>
    
  

  
    
  <li><a href="?page=5">5</a></li>
    
  

  
    
  <li><a href="?page=6">6</a></li>
    
  


  <li><a href="?page=2">»</a></li>

</ul>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
