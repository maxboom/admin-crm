
<section class="content-header">
    <h1>
        Livrare
        <small>Livrare table</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Livrare</li>
    </ol>
</section>
<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">
                    
                </div>
                <div class="box-body table-responsive">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
                                <th></th>
								<th style="width:160px;"><input class="form-control" id="id_title" maxlength="128" name="title" type="text"></th>
                                <th><select multiple="" class="form-control select2 select2-hidden-accessible" id="id_country_name" name="country_name" tabindex="-1" aria-hidden="true">
<option value="3">Румыния</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 176px;"><span class="selection"><span class="select2-selection select2-selection--multiple" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0"><ul class="select2-selection__rendered"><li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="textbox" placeholder="" style="width: 0.75em;"></li></ul></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
								<th style="min-width:100px;max-width:100px;"><input class="form-control" id="id_delivery_price" name="delivery_price" type="text"></th>
								<th style="min-width:100px;max-width:100px;"><input class="form-control" id="id_delivery_price_return" name="delivery_price_return" type="text"></th>
								<th><select class="form-control select2 select2-hidden-accessible" id="id_currency" name="currency" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="2">Польский злотый</option>
<option value="3">Евро</option>
<option value="4">Румынский лей</option>
<option value="1">Украинская гривна</option>
<option value="5">Российский рубль</option>
<option value="6">Доллар США</option>
<option value="7">Венгерский форинт</option>
<option value="8">Словацкая крона</option>
<option value="9">Болгарский лев</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 276px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_currency-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
                                <th>ID</th>
								<th>Nume</th>
                                <th>Tară</th>
								<th>Delivery price</th>
								<th>Delivery price return</th>
								<th>Currency</th>
                                <th style="width: 116px; min-width:116px;">
                                </th>
							</tr>
						</thead>
						<tbody>
						
							<tr>
								<td>4</td>
								<td>Почта Румынии</td>
                                <td>
                                        Румыния
                                        
                                    </td>
								<td>20,0</td>
								<td>20,0</td>
								<td>Румынский лей</td>
                                <td>
                                    
                                </td>
							</tr>
						
						</tbody>
					</table>

                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
<!-- /.content -->
