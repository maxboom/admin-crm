
        <script>
$(document).ready(function () {

    function next_order(){
        $('#start').prop('disabled', true);
        $.ajax({
            type: "GET",
            url: '/panel/calling/ro/orders/get_order/',
            success: function (data) {
                if (data.status == "no")
                {
                    $('#start').prop('disabled', false);
                    alert("Repeat in 5 minutes.");
                }
                else{
                   window.open("/panel/calling/ro/?id=" + data.id, "_self");
                }
            }
        });
    }

	$('#start').click(function () {
        next_order();
    });

});
</script>

<form action="" method="POST">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="box box-primary">

	            <div class="box-header">

	            </div>

                <div class="box-body">
	                <button type="button" id="start" class="btn btn-success">Start</button>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

    </div>

</section>
</form>
