
<section class="content-header">
    <h1>
	    Goods in stock
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Stock</li>
    </ol>
</section>


<div class="col-md-12">
<div class="callout callout-warning">
	<div class="row">
		<h4 class="col-xs-12">Goods checked more than 7 days ago</h4>
		
			<div class="col-md-4">
				<ul>
					
					<li>ROMANIA: BigBust Shape Up</li>
					
					<li>ROMANIA: Titan Gel</li>
					
					<li>ROMANIA: XXXL Cream</li>
					
					<li>ROMANIA: Toe Fix</li>
					
					<li>ROMANIA: Hair removal</li>
					
					<li>ROMANIA: Teeth Whitening Pen</li>
					
					<li>ROMANIA: Goji Berries</li>
					
					<li>ROMANIA: XXL Power Life</li>
					
					<li>ROMANIA: Electricity Saving Box</li>
					
				</ul>
			</div>
		
	</div>
</div>
</div>


<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">
                    <a href="/panel/goodsinstock/create/" class="btn btn-sm btn-primary">
                        <span class="fa fa-dropbox"></span> <b>New goods </b>
                    </a>
                    <a href="/panel/stock/create/" class="btn btn-sm btn-primary">
                        <span class="fa fa-home"></span> <b>New Stock </b>
                    </a>
					<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#returnsModal">
                                                            Returned orders
                                                        </button>
					<button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#sendGoodsModal">
                                                            Transfer goods
                                                        </button>
                </div>

				<div class="box-body">
					<p>Display
						<select class="select2 select2-hidden-accessible" name="paginate_in" onchange="this.form.submit()" tabindex="-1" aria-hidden="true">
							<option value="10">10</option>
							<option value="25">25</option>
							<option value="50">50</option>
						</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 41px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-paginate_in-y3-container"><span class="select2-selection__rendered" id="select2-paginate_in-y3-container" title="10">10</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
						entries</p>
				</div>
                <div class="box-body table-responsive">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
								<th>ID</th>
								<th><select class="form-control select2 select2-hidden-accessible" id="id_goods" name="goods" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="18">Electricity Saving Box</option>
<option value="22">Toe Fix</option>
<option value="21">Fito grow</option>
<option value="13">XXL Power Life</option>
<option value="5">Teeth Whitening Pen</option>
<option value="17">Семена Чиа</option>
<option value="16">Носки SOSU</option>
<option value="12">Возбудитель Silver Fox</option>
<option value="7">Магниты от курения ZeroSmoke</option>
<option value="10">«White Light» - набор для отбеливания зубов</option>
<option value="9">Имбирный кофе</option>
<option value="11">Крем для увеличения груди Bust Cream Salon Spa</option>
<option value="6">Энергетические браслеты PowerBalance</option>
<option value="4">Клипса антихрап</option>
<option value="2">Whitelight 3d White</option>
<option value="19">Мужской стимулятор Brutalin</option>
<option value="26">Акулий жир</option>
<option value="25">Гриб Рейша</option>
<option value="24">Шоко Слим</option>
<option value="23">Молот Тора</option>
<option value="15">Жидкий каштан (Гуарана)</option>
<option value="27">Мужские капли Bigzilla</option>
<option value="31">Alcachofa</option>
<option value="36">Годжи крем</option>
<option value="29">Forskolin</option>
<option value="28">Spirulina</option>
<option value="32">Garcinia</option>
<option value="33">Гранатовая эмульсия</option>
<option value="34">Морковная маска</option>
<option value="30">Excite Power Plus</option>
<option value="35">Провокация крем</option>
<option value="37">Hair removal</option>
<option value="38">Активатор рыбного клева</option>
<option value="8">Green Coffee</option>
<option value="39">XXXL Cream</option>
<option value="40">ПБК-20</option>
<option value="41">VaryForte</option>
<option value="42">OsteoRen</option>
<option value="43">Jarabe Dren</option>
<option value="44">Liquid Stop</option>
<option value="45">Saciante</option>
<option value="1">Goji Berries</option>
<option value="14">Toe PRO</option>
<option value="3">Breast Cream</option>
<option value="20">Antiaging cream</option>
<option value="47">Titan Gel</option>
<option value="46">MaxLift Day</option>
<option value="48">MaxLift Night</option>
<option value="49">VaryForte +</option>
<option value="50">Forskolin 2</option>
<option value="51">Braslet Xpower </option>
<option value="52">Heart of ocean</option>
<option value="53">Toretto Necklace </option>
<option value="54">Thor's Hammer</option>
<option value="55">Fungalor</option>
<option value="56">Fixline Detox </option>
<option value="57">Fixline  Weight Control </option>
<option value="59">BigBust Firming Up</option>
<option value="58">BigBust Shape Up</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 366px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_goods-container"><span class="select2-selection__rendered" id="select2-id_goods-container" title="---------">---------</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
								<th><select class="form-control select2 select2-hidden-accessible" id="id_stock" name="stock" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="3">ROMANIA</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 104px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_stock-container"><span class="select2-selection__rendered" id="select2-id_stock-container" title="---------">---------</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
								<th></th>
								
								<th></th>
								<th></th>
								
								<th></th>
								<th colspan="3">anaytic</th>
                                <th></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
								<th>Goods ID</th>
								<th>Goods title</th>
								<th>Stock</th>
								<th>Suma</th>
								<!--<th>Возвраты</th>-->
								
								<th>Preț</th>
								<th>Отправки</th>
								
								<th>Ожидается</th>
								<th>Days left</th>
								<th>Avarage</th>
								<th>Yestarday</th>
								<th>Last check</th>
                                <th style="width: 116px; min-width:116px;">
                                </th>
							</tr>
						</thead>
						<tbody>
						
							<tr style="background: rgba(201, 35, 45, 0.5);">
								<td>58</td>
								<td><a href="/panel/goods/edit/58/">BigBust Shape Up</a></td>
								<td><a href="/panel/stock/edit/3">ROMANIA</a></td>
								<td>0</td>
								<td>0,0</td>
								<!--<td>0</td>-->
								<td>
									
										0
									
								</td>
						
								<td>
									
										-
									
								</td>
								<td>0</td>
								<td>0,0</td>
								<td>0</td>
								<td></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/panel/goodsinstock/edit/150" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr style="background: rgba(201, 35, 45, 0.5);">
								<td>47</td>
								<td><a href="/panel/goods/edit/47/">Titan Gel</a></td>
								<td><a href="/panel/stock/edit/3">ROMANIA</a></td>
								<td>0</td>
								<td>0,0</td>
								<!--<td>0</td>-->
								<td>
									
										0
									
								</td>
						
								<td>
									
										<!---->
										<i class="fa fa-clock-o" data-toggle="modal" data-target="#id_good_132"> 2000</i>
										<div class="modal fade" id="id_good_132" role="dialog">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">×</span></button>
														<h4>ROMANIA: Titan Gel </h4>
													</div>
													<div class="modal-body">
														<table class="table table-hover transfer">
															<tbody><tr>
																<th>Arrival date</th>
																<th>Count</th>
																<th>From</th>
																<th>Complete transfer</th>
															</tr>
															
																<tr>
																	<td>2016-02-22</td>
																	<td>2000</td>
																	<td>SPAIN</td>
																	<td><div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="confirm_transfer_box" type="checkbox" value="4" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
																</tr>
															
														</tbody></table>
													</div>

													<div class="modal-footer">
														<input type="button" class="btn btn-primary" onclick="confirmTransfer()" name="confirm_transfer" value="Confirm transfers">
														<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
													</div>

												</div>
											</div>
										</div>

									
								</td>
								<td>0,0</td>
								<td>103,29</td>
								<td>0</td>
								<td></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/panel/goodsinstock/edit/132" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>42</td>
								<td><a href="/panel/goods/edit/42/">OsteoRen</a></td>
								<td><a href="/panel/stock/edit/3">ROMANIA</a></td>
								<td>871</td>
								<td>3048,5</td>
								<!--<td>0</td>-->
								<td>
									
										0
									
								</td>
						
								<td>
									
										-
									
								</td>
								<td>43,86</td>
								<td>19,86</td>
								<td>21</td>
								<td>2016-02-16</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/panel/goodsinstock/edit/115" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>32</td>
								<td><a href="/panel/goods/edit/32/">Garcinia</a></td>
								<td><a href="/panel/stock/edit/3">ROMANIA</a></td>
								<td>2000</td>
								<td>7000,0</td>
								<!--<td>0</td>-->
								<td>
									
										0
									
								</td>
						
								<td>
									
										-
									
								</td>
								<td>2000</td>
								<td>0,0</td>
								<td>0</td>
								<td>2016-02-16</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/panel/goodsinstock/edit/111" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr style="background: rgba(201, 35, 45, 0.5);">
								<td>41</td>
								<td><a href="/panel/goods/edit/41/">VaryForte</a></td>
								<td><a href="/panel/stock/edit/3">ROMANIA</a></td>
								<td>0</td>
								<td>0,0</td>
								<!--<td>0</td>-->
								<td>
									
										0
									
								</td>
						
								<td>
									
										-
									
								</td>
								<td>0,0</td>
								<td>86,14</td>
								<td>56</td>
								<td>2016-02-16</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/panel/goodsinstock/edit/110" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr style="background: rgba(201, 35, 45, 0.5);">
								<td>39</td>
								<td><a href="/panel/goods/edit/39/">XXXL Cream</a></td>
								<td><a href="/panel/stock/edit/3">ROMANIA</a></td>
								<td>0</td>
								<td>0,0</td>
								<!--<td>0</td>-->
								<td>
									
										0
									
								</td>
						
								<td>
									
										-
									
								</td>
								<td>0</td>
								<td>0,0</td>
								<td>0</td>
								<td></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/panel/goodsinstock/edit/105" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>22</td>
								<td><a href="/panel/goods/edit/22/">Toe Fix</a></td>
								<td><a href="/panel/stock/edit/3">ROMANIA</a></td>
								<td>1991</td>
								<td>5973,0</td>
								<!--<td>0</td>-->
								<td>
									
										0
									
								</td>
						
								<td>
									
										-
									
								</td>
								<td>44,67</td>
								<td>44,57</td>
								<td>22</td>
								<td></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/panel/goodsinstock/edit/89" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>37</td>
								<td><a href="/panel/goods/edit/37/">Hair removal</a></td>
								<td><a href="/panel/stock/edit/3">ROMANIA</a></td>
								<td>9349</td>
								<td>28047,0</td>
								<!--<td>0</td>-->
								<td>
									
										0
									
								</td>
						
								<td>
									
										-
									
								</td>
								<td>9349</td>
								<td>0,43</td>
								<td>0</td>
								<td></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/panel/goodsinstock/edit/88" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>20</td>
								<td><a href="/panel/goods/edit/20/">Antiaging cream</a></td>
								<td><a href="/panel/stock/edit/3">ROMANIA</a></td>
								<td>876</td>
								<td>2628,0</td>
								<!--<td>0</td>-->
								<td>
									
										0
									
								</td>
						
								<td>
									
										-
									
								</td>
								<td>13,33</td>
								<td>65,71</td>
								<td>37</td>
								<td>2016-02-16</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/panel/goodsinstock/edit/66" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>5</td>
								<td><a href="/panel/goods/edit/5/">Teeth Whitening Pen</a></td>
								<td><a href="/panel/stock/edit/3">ROMANIA</a></td>
								<td>2627</td>
								<td>7881,0</td>
								<!--<td>0</td>-->
								<td>
									
										0
									
								</td>
						
								<td>
									
										-
									
								</td>
								<td>92,41</td>
								<td>28,43</td>
								<td>6</td>
								<td></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="/panel/goodsinstock/edit/65" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
						</tbody>
					</table>

				


<ul class="pagination">

  <li class="disabled"><a href="#" onclick="javascript: return false;">«</a></li>


  
    
  <li class="active"><a href="#" onclick="javascript: return false;">1</a></li>
    
  

  
    
  <li><a href="?page=2">2</a></li>
    
  


  <li><a href="?page=2">»</a></li>

</ul>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>

<form action="" method="post" class="form-horizontal" role="form">
	<!---->
	<div class="modal fade" id="returnsModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Returned packages</h4>
				</div>
				<div class="modal-body">
					<h5>Enter trackers of returned packages (you can scan them from invoices)</h5>
					<p><textarea class="form-control" cols="40" name="returns" rows="10"></textarea></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary" name="save_changes" value="Save changes">
				</div>

			</div>
		</div>
	</div>
</form>


<form action="" method="post">
	<div class="modal fade" id="sendGoodsModal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Transfer goods</h4>
				</div>

				<div class="modal-body">

					<section class="content">
						<div class="row">
							<div class="col-md-12 col-xs-12">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group ">
											<label>From</label><br>
											
											<select class="form-control select2 select2-hidden-accessible" id="id_from_stock" name="from_stock" style="width:100%;" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="3">ROMANIA</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_from_stock-container"><span class="select2-selection__rendered" id="select2-id_from_stock-container" title="---------">---------</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<div class="form-group ">
											<label>Goods</label><br>
											
											<select class="form-control select2 select2-hidden-accessible" id="id_good" name="good" style="width:100%;" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="18">Electricity Saving Box</option>
<option value="22">Toe Fix</option>
<option value="21">Fito grow</option>
<option value="13">XXL Power Life</option>
<option value="5">Teeth Whitening Pen</option>
<option value="17">Семена Чиа</option>
<option value="16">Носки SOSU</option>
<option value="12">Возбудитель Silver Fox</option>
<option value="7">Магниты от курения ZeroSmoke</option>
<option value="10">«White Light» - набор для отбеливания зубов</option>
<option value="9">Имбирный кофе</option>
<option value="11">Крем для увеличения груди Bust Cream Salon Spa</option>
<option value="6">Энергетические браслеты PowerBalance</option>
<option value="4">Клипса антихрап</option>
<option value="2">Whitelight 3d White</option>
<option value="19">Мужской стимулятор Brutalin</option>
<option value="26">Акулий жир</option>
<option value="25">Гриб Рейша</option>
<option value="24">Шоко Слим</option>
<option value="23">Молот Тора</option>
<option value="15">Жидкий каштан (Гуарана)</option>
<option value="27">Мужские капли Bigzilla</option>
<option value="31">Alcachofa</option>
<option value="36">Годжи крем</option>
<option value="29">Forskolin</option>
<option value="28">Spirulina</option>
<option value="32">Garcinia</option>
<option value="33">Гранатовая эмульсия</option>
<option value="34">Морковная маска</option>
<option value="30">Excite Power Plus</option>
<option value="35">Провокация крем</option>
<option value="37">Hair removal</option>
<option value="38">Активатор рыбного клева</option>
<option value="8">Green Coffee</option>
<option value="39">XXXL Cream</option>
<option value="40">ПБК-20</option>
<option value="41">VaryForte</option>
<option value="42">OsteoRen</option>
<option value="43">Jarabe Dren</option>
<option value="44">Liquid Stop</option>
<option value="45">Saciante</option>
<option value="1">Goji Berries</option>
<option value="14">Toe PRO</option>
<option value="3">Breast Cream</option>
<option value="20">Antiaging cream</option>
<option value="47">Titan Gel</option>
<option value="46">MaxLift Day</option>
<option value="48">MaxLift Night</option>
<option value="49">VaryForte +</option>
<option value="50">Forskolin 2</option>
<option value="51">Braslet Xpower </option>
<option value="52">Heart of ocean</option>
<option value="53">Toretto Necklace </option>
<option value="54">Thor's Hammer</option>
<option value="55">Fungalor</option>
<option value="56">Fixline Detox </option>
<option value="57">Fixline  Weight Control </option>
<option value="59">BigBust Firming Up</option>
<option value="58">BigBust Shape Up</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_good-container"><span class="select2-selection__rendered" id="select2-id_good-container" title="---------">---------</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<div class="form-group ">
											<label>Arrive date</label>
											
											<div class="input-group">
												<div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												</div>
												<input class="form-control datepicker datepicker-z" data-date-format="yyyy-mm-dd" id="id_date_arrive" name="date_arrive" type="text">
											</div>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="form-group ">
											<label>To</label><br>
											
											<select class="form-control select2 select2-hidden-accessible" id="id_to_stock" name="to_stock" style="width:100%;" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="8">ITALY</option>
<option value="7">HUNGARY</option>
<option value="6">SPAIN</option>
<option value="5">GREECE</option>
<option value="4">POLAND</option>
<option value="3">ROMANIA</option>
<option value="2">RUSSIA</option>
<option value="1">UKRAINE</option>
<option value="14">BULGARIA</option>
<option value="12">CYPRUS</option>
<option value="15">MANUFACTURE</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_to_stock-container"><span class="select2-selection__rendered" id="select2-id_to_stock-container" title="---------">---------</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
										</div>
										<div class="form-group ">
											<label>Count</label><br>
											
											<input class="form-control" id="id_count" name="count" type="number">
										</div>

									</div>
								</div>
							</div>
						</div>
					</section>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary" name="confirm" value="Confirm">
				</div>

			</div>
		</div>
	</div>
</form>
<!-- /.content -->

<script>
	function confirmTransfer(){
		var toConfirm = [];
		$('.confirm_transfer_box:checked').each(function(){
			toConfirm.push(this.value);
		});
		$.ajax({
			url: '/api/stock_transfer/confirm_transfer/?data=' + toConfirm,
			type: 'GET',
			dataType: 'json',
			async: false,
			success: function (data) {
			}
		});
		location.reload(true);
	}

	function confirmPayment(){
		var toConfirm = [];
		$('.confirm_payment_transfer_box:checked').each(function(){
			toConfirm.push(this.value);
		});
		$.ajax({
			url: '/api/stock_transfer/confirm_payment/?data=' + toConfirm,
			type: 'GET',
			dataType: 'json',
			async: false,
			success: function (data) {
			}
		});
		location.reload(true);
	}

	$(document).ready(function(){
		$('#id_from_stock').change(function(){
        var from_stock = $(this).val();
        var goods = $("#id_good");
        goods.empty();
        if (goods.length && from_stock != ""){
            $.ajax({
                url: '/api/stock_transfer/get_goods/?from_stock=' + from_stock,
                type: 'GET',
                dataType: 'json',
                success: function(data){
                    var toAppend = '<option selected value="">---------</option>';
                    $.each(data, function(i,o){
                        toAppend += '<option value="' + o.id + '">'+o.name+'</option>';
                    });
                    goods.append(toAppend);
                    goods.val("");
                    goods.select2();
                }
            });
        }
    	});
	});
</script>
