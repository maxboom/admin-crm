
<section class="content-header">
    <h1>
        Orders
        <small>Orders to send ro</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Orders</li>
    </ol>
</section>

<form id="packing">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">
                    <div class="btn-group">
                        <button data-url="/panel/packing/ro/get_trackers/" id="get_trackers" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-barcode"></span> Get Trackers</button>
                    </div>
                    <div class="btn-group">
                        <button data-url="/panel/packing/ro/delete_trackers/" id="delete_trackers" type="button" class="btn btn-warning"><span class="glyphicon glyphicon-trash"></span> Delete Trackers</button>
                    </div>
                    <div class="btn-group">
                        <button data-url="/panel/packing/ro/reject/" id="reject" type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Reject</button>
                    </div>
                    <div class="btn-group">
                        <button data-url="/panel/packing/ro/print_invoice/?" id="print" type="button" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print Invoce</button>
                    </div>
                    <div class="btn-group">
                        <button data-url="/panel/packing/ro/print_fan/?" id="print_fan" type="button" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print FAN</button>
                    </div>
                    <div class="btn-group">
                        <button data-url="/panel/packing/shipped/" id="shipped" type="button" class="btn btn-success"><span class="glyphicon glyphicon-code"></span> Shipped</button>
                    </div>
                </div>

                <div class="box-body table-responsive">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
                                <th>
                                    <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" id="check_all" name="check_all" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                </th>
								<th style="min-width:100px; max-width:100px;"><input value="" name="id" class="form-control" type="text"></th>
								<th></th>
								<th><input class="form-control" id="id_trackid" maxlength="128" name="trackid" type="text"></th>
								<th><input class="form-control" id="id_name" maxlength="1024" name="name" type="text"></th>
								<th></th>

								<th style="width:80px;"></th>

								<th></th>
								<th style="min-width:100px; max-width:100px;">
									<select class="form-control select2 select2-hidden-accessible" id="id_delivery" name="delivery" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="4">Почта Румынии</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 84px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_delivery-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
								</th>
                                <th></th>
								<th style="min-width:100px; max-width:100px;"></th>
								<th style="min-width:100px; max-width:100px;"></th>
								<th style="min-width:100px; max-width:100px;"></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
								<th>Ch</th>
								<th>ID</th>
								<th>Goods</th>
								<th>TrackId</th>
								<th>Name</th>

								<th>Dep.</th>
								<th>Preț</th>

								<th>Oraș</th>
								<th>Livrare</th>
								<th>Delivery price</th>
								<th>Created</th>
								<th>Modified</th>
								<th>Send</th>
                                <th></th>
							</tr>
						</thead>
						<tbody>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553713" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553713</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553713" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553713"></span>
								</td>
								<td>Mirica Adrian</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>160,0 / 1</td>

								<td>Lunguletu</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									6:51
								</td>
								<td>
									2016-02-16<br>
									10:12
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553713" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553749" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553749</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553749" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553749"></span>
								</td>
								<td>Marius</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Strejesti</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									7:26
								</td>
								<td>
									2016-02-16<br>
									10:16
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553749" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553766" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553766</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553766" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553766"></span>
								</td>
								<td>HERMAN IOAN STEFAN</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>219,0 / 2</td>

								<td>Sebes</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									7:44
								</td>
								<td>
									2016-02-16<br>
									10:16
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553766" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="552697" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>552697</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-552697" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-552697"></span>
								</td>
								<td>Potop ionut</td>

								<td> </td>
								<td>160,0 / 1</td>

								<td>Vidra</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-15<br>
									21:14
								</td>
								<td>
									2016-02-16<br>
									9:18
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/552697" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="552764" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>552764</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-552764" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-552764"></span>
								</td>
								<td>Sandu Petrisor</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Bals</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-15<br>
									21:36
								</td>
								<td>
									2016-02-16<br>
									9:24
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/552764" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="552831" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>552831</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-552831" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-552831"></span>
								</td>
								<td>dobarcean ioan</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Uricani</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-15<br>
									21:59
								</td>
								<td>
									2016-02-16<br>
									9:29
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/552831" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="552787" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>552787</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-552787" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-552787"></span>
								</td>
								<td>Carstea Ion</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Oradea</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-15<br>
									21:43
								</td>
								<td>
									2016-02-16<br>
									9:32
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/552787" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="552832" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>552832</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-552832" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-552832"></span>
								</td>
								<td>Savu Viorel</td>

								<td> </td>
								<td>160,0 / 1</td>

								<td>Bucuresti</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-15<br>
									21:59
								</td>
								<td>
									2016-02-16<br>
									9:32
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/552832" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="552886" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>552886</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-552886" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-552886"></span>
								</td>
								<td>Varg</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>160,0 / 1</td>

								<td>Alesd</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-15<br>
									22:17
								</td>
								<td>
									2016-02-16<br>
									9:38
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/552886" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553175" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553175</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553175" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553175"></span>
								</td>
								<td>Butnaru Daniel</td>

								<td> </td>
								<td>160,0 / 1</td>

								<td>Techirghiol</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-15<br>
									23:37
								</td>
								<td>
									2016-02-16<br>
									9:52
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553175" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553196" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553196</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553196" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553196"></span>
								</td>
								<td>Szeiman Alexandru</td>

								<td> </td>
								<td>160,0 / 1</td>

								<td>Andrei Saguna</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-15<br>
									23:45
								</td>
								<td>
									2016-02-16<br>
									9:52
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553196" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553278" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553278</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553278" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553278"></span>
								</td>
								<td>iacoboaiea iulian</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Viroaga</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									0:10
								</td>
								<td>
									2016-02-16<br>
									14:12
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553278" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553482" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553482</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553482" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553482"></span>
								</td>
								<td>Usurelu</td>

								<td> </td>
								<td>160,0 / 1</td>

								<td>Bucuresti</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									1:36
								</td>
								<td>
									2016-02-16<br>
									10:01
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553482" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553554" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553554</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553554" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553554"></span>
								</td>
								<td>Namoc onimis</td>

								<td> </td>
								<td>160,0 / 1</td>

								<td>Lugoj</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									2:15
								</td>
								<td>
									2016-02-16<br>
									10:04
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553554" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553527" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553527</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553527" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553527"></span>
								</td>
								<td>Petran Paul</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Prundu Bargaului</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									1:58
								</td>
								<td>
									2016-02-16<br>
									16:59
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553527" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553585" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553585</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553585" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553585"></span>
								</td>
								<td>Curta Radu</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Sibiu</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									2:48
								</td>
								<td>
									2016-02-16<br>
									10:08
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553585" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553709" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553709</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553709" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553709"></span>
								</td>
								<td>Cornel Carasca</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Geoagiu</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									6:46
								</td>
								<td>
									2016-02-16<br>
									10:11
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553709" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553973" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553973</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553973" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553973"></span>
								</td>
								<td>Duia</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>219,0 / 2</td>

								<td>Buzau</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									10:12
								</td>
								<td>
									2016-02-16<br>
									16:38
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553973" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553977" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553977</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553977" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553977"></span>
								</td>
								<td>Costel</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>219,0 / 2</td>

								<td>Iasi</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									10:12
								</td>
								<td>
									2016-02-16<br>
									14:37
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553977" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553986" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553986</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553986" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553986"></span>
								</td>
								<td>ICHIM GHEORGHE-ADRIAN</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Vanatori</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									10:20
								</td>
								<td>
									2016-02-16<br>
									10:28
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553986" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553975" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553975</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553975" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553975"></span>
								</td>
								<td>Sacara Alexandru</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Cluj-Napoca</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									10:12
								</td>
								<td>
									2016-02-16<br>
									10:27
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553975" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="553992" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>553992</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-553992" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-553992"></span>
								</td>
								<td>nicolae georgian</td>

								<td> </td>
								<td>160,0 / 1</td>

								<td>Stefan Voda</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									10:20
								</td>
								<td>
									2016-02-16<br>
									10:30
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/553992" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="554031" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>554031</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-554031" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-554031"></span>
								</td>
								<td>Iosiv Laurentiu</td>

								<td> </td>
								<td>160,0 / 1</td>

								<td>Garoafa</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									10:36
								</td>
								<td>
									2016-02-16<br>
									10:42
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/554031" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="554025" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>554025</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-554025" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-554025"></span>
								</td>
								<td>NECHITA VASILE</td>

								<td> </td>
								<td>160,0 / 1</td>

								<td>Anina</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									10:36
								</td>
								<td>
									2016-02-16<br>
									10:43
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/554025" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="554045" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>554045</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-554045" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-554045"></span>
								</td>
								<td>Baloiu Constantin</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Targu Jiu</td>
								<td>Почта Румынии</td>
								<td>20,0</td>
								<td>
									2016-02-16<br>
									10:44
								</td>
								<td>
									2016-02-16<br>
									10:45
								</td>
								<td>
									2016-02-16<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/554045" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
						</tbody>
					</table>

                    


<ul class="pagination">

  <li class="disabled"><a href="#" onclick="javascript: return false;">«</a></li>


  
    
  <li class="active"><a href="#" onclick="javascript: return false;">1</a></li>
    
  

  
    
  <li><a href="?page=2">2</a></li>
    
  

  
    
  <li><a href="?page=3">3</a></li>
    
  

  
    
  <li><a href="?page=4">4</a></li>
    
  

  
  <li class="disabled"><a href="#" onclick="javascript: return false;">...</a></li>
  

  
    
  <li><a href="?page=35">35</a></li>
    
  

  
    
  <li><a href="?page=36">36</a></li>
    
  

  
    
  <li><a href="?page=37">37</a></li>
    
  

  
    
  <li><a href="?page=38">38</a></li>
    
  


  <li><a href="?page=2">»</a></li>

</ul>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
