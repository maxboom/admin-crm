<section class="content">
    <div class="row">
        <div class="col-md-9 col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
	                <div class="row">
		                <div class="col-md-6">
							<div class="form-group">
								
								
						    </div>
							<div class="form-group ">
							    <label>Status</label>
							    
							    <select class="form-control select2 select2-hidden-accessible" id="id_status" name="status" tabindex="-1" aria-hidden="true">
                                <option value="">---------</option>
                                <option value="0" selected="selected">Pending</option>
                                <option value="11">Busy</option>
                                <option value="12">Denied</option>
                                <option value="13">Duplicate</option>
                                <option value="14">Incorrect</option>
                                <option value="20">Accepted</option>
                                <option value="21">Shipped</option>
                                <option value="22">At department</option>
                                <option value="23">Paid</option>
                                <option value="24">Return</option>
                                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 447px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_status-container"><span class="select2-selection__rendered" id="select2-id_status-container" title="Pending">Pending</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
							</div>
							<div class="form-group ">
							    <label>Track ID</label>
							    
							    <input class="form-control" id="id_trackid" maxlength="128" name="trackid" type="text">
							</div>
							<div class="form-group ">
							    <label>Livrare</label>
							    
							    <select class="form-control select2 select2-hidden-accessible" id="id_delivery" name="delivery" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="4">Почта Румынии</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 447px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_delivery-container"><span class="select2-selection__rendered" id="select2-id_delivery-container" title="---------">---------</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
							</div>









							<div class="form-group ">
							    <label>Comentariu</label>
							    
							    <textarea class="form-control" cols="40" id="id_comment" name="comment" rows="3"></textarea>
							</div>
							<div class="form-group ">
							    <label>Delivery price</label>
							    
							    <input class="form-control" id="id_delivery_courier_price" name="delivery_courier_price" type="number" value="0.0">
							</div>
							<div class="form-group ">
							    <label>TEST total_order_price</label>
							    
							    <input class="form-control" id="id_total_order_price" name="total_order_price" readonly="" type="number" value="0.0">
							</div>


                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Nume</label>
                                    <input class="form-control" id="good" value="" disabled="">
                                </div>
                                <div class="col-sm-3">
                                    <label>Suma</label>
	                                <input class="form-control" value="1" disabled="">
                                </div>
                                <div class="col-sm-3">
                                    <label>Preț</label>
                                    <input class="form-control" value="140,0" disabled="">
                                </div>
                            </div><br>
                            
	                    </div>

		                <div class="col-md-6">
						    <div class="form-group">
								
						    </div>
		                    <div class="form-group ">
		                        <label>Tară</label>
			                    
			                    <select class="form-control select2 select2-hidden-accessible" id="id_country" name="country" tabindex="-1" aria-hidden="true">
<option value="">---------</option>
<option value="3" selected="selected">Румыния</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 447px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_country-container"><span class="select2-selection__rendered" id="select2-id_country-container" title="Румыния">Румыния</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
		                    </div>
		                    <div class="form-group ">
		                        <label>State</label>
			                    
			                    <select name="state" id="id_state" class="form-control  select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true">
				                        <option value="">---------</option>
				                    
					                    
					                    <option value="57">Alba</option>
					                    
				                    
					                    
					                    <option value="58">Arad</option>
					                    
				                    
					                    
					                    <option value="59">Arges</option>
					                    
				                    
					                    
					                    <option value="60">Bacau</option>
					                    
				                    
					                    
					                    <option value="61">Bihor</option>
					                    
				                    
					                    
					                    <option value="62">Bistrita-Nasaud</option>
					                    
				                    
					                    
					                    <option value="63">Botosani</option>
					                    
				                    
					                    
					                    <option value="64">Braila</option>
					                    
				                    
					                    
					                    <option value="65">Brasov</option>
					                    
				                    
					                    
					                    <option value="66">Bucuresti</option>
					                    
				                    
					                    
					                    <option value="67">Buzau</option>
					                    
				                    
					                    
					                    <option value="68">Calarasi</option>
					                    
				                    
					                    
					                    <option value="69">Caras-Severin</option>
					                    
				                    
					                    
					                    <option value="70">Cluj</option>
					                    
				                    
					                    
					                    <option value="71">Constanta</option>
					                    
				                    
					                    
					                    <option value="72">Covasna</option>
					                    
				                    
					                    
					                    <option value="73">Dambovita</option>
					                    
				                    
					                    
					                    <option value="74">Dolj</option>
					                    
				                    
					                    
					                    <option value="75">Galati</option>
					                    
				                    
					                    
					                    <option value="76">Giurgiu</option>
					                    
				                    
					                    
					                    <option value="77">Gorj</option>
					                    
				                    
					                    
					                    <option value="78">Harghita</option>
					                    
				                    
					                    
					                    <option value="79">Hunedoara</option>
					                    
				                    
					                    
					                    <option value="80">Ialomita</option>
					                    
				                    
					                    
					                    <option value="81">Iasi</option>
					                    
				                    
					                    
					                    <option value="98">Ilfov</option>
					                    
				                    
					                    
					                    <option value="82">Maramures</option>
					                    
				                    
					                    
					                    <option value="83">Mehedinti</option>
					                    
				                    
					                    
					                    <option value="84">Mures</option>
					                    
				                    
					                    
					                    <option value="85">Neamt</option>
					                    
				                    
					                    
					                    <option value="86">Olt</option>
					                    
				                    
					                    
					                    <option value="87">Prahova</option>
					                    
				                    
					                    
					                    <option value="88">Salaj</option>
					                    
				                    
					                    
					                    <option value="89">Satu Mare</option>
					                    
				                    
					                    
					                    <option value="90">Sibiu</option>
					                    
				                    
					                    
					                    <option value="91">Suceava</option>
					                    
				                    
					                    
					                    <option value="92">Teleorman</option>
					                    
				                    
					                    
					                    <option value="93">Timis</option>
					                    
				                    
					                    
					                    <option value="94">Tulcea</option>
					                    
				                    
					                    
					                    <option value="95">Valcea</option>
					                    
				                    
					                    
					                    <option value="96">Vaslui</option>
					                    
				                    
					                    
					                    <option value="97">Vrancea</option>
					                    
				                    
			                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 447px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_state-container"><span class="select2-selection__rendered" id="select2-id_state-container" title="---------">---------</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
		                    </div>
		                    <div class="form-group ">
		                        <label>Oraș</label>
			                    
			                    <select class="form-control select2 select2-hidden-accessible" id="id_city" name="city" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 447px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_city-container"><span class="select2-selection__rendered" id="select2-id_city-container" title="---------">---------</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
		                    </div>
							
		                    <div class="form-group ">
		                        <label>FIO</label>
			                    
			                    <input class="form-control" id="id_name" maxlength="1024" name="name" type="text" value="">
		                    </div>
		                    <div class="form-group ">
		                        <label>Telefon</label>
			                    
			                    <input class="form-control" id="id_phone" maxlength="512" name="phone" type="text" value="0765306407">
		                    </div>
		                    <div class="form-group ">
		                        <label>Shipp adress</label>
			                    
			                    <input class="form-control" id="id_shipaddress" maxlength="1024" name="shipaddress" type="text" value="">
		                    </div>
		                    <div class="form-group ">
		                        <label>Block</label>
			                    
			                    <input class="form-control" id="id_building" maxlength="1024" name="building" type="text">
		                    </div>
		                    <div class="form-group ">
		                        <label>Apartment</label>
			                    
			                    <input class="form-control" id="id_apartment" maxlength="1024" name="apartment" type="text">
		                    </div>
		                    <div class="form-group ">
		                        <label>Zipcode</label>
			                    
			                    <input class="form-control" id="id_zipcode" maxlength="1024" name="zipcode" type="text">
		                    </div>
		                    
		                    

		                    
		                    
		                    
		                    

			            </div>
	                </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <!--col-->
        <div class="col-md-3 col-xs-12">
            <div class="box box-primary">
                 <!--/.box-header -->
                <div class="box-header">
                    <h3 class="box-title">Acțiune</h3>
                </div>
                 <!--/.box-body -->
                <div class="box-body">
                    <div class="form-group">
                        <button name="action" value="save_stay" type="submit" class="btn btn-block btn-block-menu btn-success">
                            <span class="glyphicon glyphicon-floppy-saved"></span> <b>SAVE AND STAY </b>
                        </button>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <div class="box box-primary">
                 <!--/.box-header -->
                <div class="box-header">
                    <h3 class="box-title">Information</h3>
                </div>
                 <!--/.box-body -->
                <div class="box-body">
                    <div class="form-group">
                        <a href="#" data-toggle="modal" data-target="#history">Istoric</a>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Order ID</label>
                                <input value="<?=$_GET['id']?>" disabled="disabled" type="text" class="form-control">
                            </div>
                            <div class="col-md-6">
                                <label>Client ID</label>
                                <input value="553374" disabled="disabled" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div style="margin-left: -18px;" class="checkbox">
                                        <label>
                                            <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input id="id_order_new_type" name="order_new_type" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> New Type
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div style="margin-left: -18px;" class="checkbox">
                                        <label>
                                            <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input id="id_exact_time" name="exact_time" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> exact_time
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div style="margin-left: -18px;" class="checkbox">
                                        <label>
                                            <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input id="id_island" name="island" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> Остров
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div style="margin-left: -18px;" class="checkbox">
                                        <label>
                                            <div class="icheckbox_minimal-green checked" aria-checked="false" aria-disabled="false" style="position: relative;"><input checked="checked" id="id_processing" name="processing" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> Processing
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div style="margin-left: -18px;" class="checkbox">
                                        <label>
                                            <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input id="id_department" name="department" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div> To dep.
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

					<div class="form-group ">
						<label>Date to send:</label>
                        
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
                            <input class="form-control  datepicker" data-date-format="yyyy-mm-dd" id="id_date_send" name="date_send" type="text">

						</div><!-- /.input group -->
					</div>
					<div class="form-group ">
						<label>Call time:</label>
                        
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
	                          <input class="form-control datetimepicker" id="id_call_time" name="call_time" type="text" value="2016-02-29 09:42">

						</div><!-- /.input group -->
					</div>
					<div class="form-group">
						<label>Created:</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input value="2016-02-29 9:42" data-date-format="yyyy-mm-dd" disabled="disabled" type="text" class="form-control pull-right">
						</div><!-- /.input group -->
					</div>
					<div class="form-group">
						<label>Modified:</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input value="2016-02-29 11:42" data-date-format="yyyy-mm-dd" disabled="disabled" type="text" class="form-control pull-right">
						</div><!-- /.input group -->
					</div>
					<div class="form-group ">
						<label>Tracker get date:</label>
						<div class="input-group">
		                    
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input value="" data-date-format="yyyy-mm-dd" disabled="disabled" type="text" class="form-control pull-right">
						</div><!-- /.input group -->
					</div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>invoice</label>
                                <input class="form-control" id="id_invoice" maxlength="128" name="invoice" readonly="readonly" type="text">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label>Call Count</label>
                                    
                                    <input class="form-control" id="id_call_count" name="call_count" readonly="readonly" type="number" value="0">
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <!-- /.box -->
        </div>
        <!--end col-->
    </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script>
$( document ).ready(function() {

      var id = '<?=$_GET['id']?>';

      $.ajax({
        url: "views/ajax/getinfo.php",
        type: "POST",
        data:'id='+id,

        success: function(result) {
          var result = JSON.parse(result);
          console.log(result);
          $('#id_name').val(result.name);
          $('#id_phone').val(result.phone);
          $('#id_shipaddress').val(result.adress);
          $('#good').val(result.goods);
          $('#id_status option:selected').text(result.status);

        }
      });
    });

$(".btn-success").click(function() {
console.log("click");
			var id = '<?=$_GET['id']?>';

            var status = $('#id_status :selected').text();

            console.log(id, status);
            $.ajax({
            type: "POST", //Метод отправки
            url: "views/ajax/update_status.php", //путь до php фаила отправителя
            data: 'id='+id+'&status='+status,
            success: function(result) {
              alert("Success");
            }
        });
         return false;
    });



</script>