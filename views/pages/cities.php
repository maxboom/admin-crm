<section class="content-header">
    <h1>
	    Cities
        <small>Cities</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Cities</li>
    </ol>
</section>

<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">
                    <a href="/panel/cities/create/" class="btn btn-sm btn-primary">
                        <span class="glyphicon glyphicon-new-window"></span> <b>Add item </b>
                    </a>
                </div>
                <div class="box-body table-responsive">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
								<th><input class="form-control" id="id_name" maxlength="128" name="name" type="text"></th>
								<th><select class="form-control select2 select2-hidden-accessible" id="id_country" name="country" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="3">Румыния</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 276px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_country-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
								<th>
			                    <select name="state" id="id_state">
				                    <option value="">---------</option>
				                    
			                    </select>
								</th>
                                <th>Area</th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
								<th>Oraș</th>
								<th>Tară</th>
								<th>State</th>
								<th>Area</th>
                                <th style="width: 116px; min-width:116px;">
                                </th>
							</tr>
						</thead>
						<tbody>
						
							<tr>
								<td>Valcanneto</td>
								<td><a href="/panel/countries/edit/9/">Италия</a></td>
								<td>ROMA</td>
								<td>ROMA</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/cities/edit/136865" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Heimsbrunn</td>
								<td><a href="/panel/countries/edit/13/">Франция</a></td>
								<td>Alsace</td>
								<td>Alsace</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/cities/edit/136864" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Galfingue</td>
								<td><a href="/panel/countries/edit/13/">Франция</a></td>
								<td>Alsace</td>
								<td>Alsace</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/cities/edit/136863" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Beblenheim</td>
								<td><a href="/panel/countries/edit/13/">Франция</a></td>
								<td>Alsace</td>
								<td>Alsace</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/cities/edit/136862" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Guémar</td>
								<td><a href="/panel/countries/edit/13/">Франция</a></td>
								<td>Alsace</td>
								<td>Alsace</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/cities/edit/136861" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Illhaeusern</td>
								<td><a href="/panel/countries/edit/13/">Франция</a></td>
								<td>Alsace</td>
								<td>Alsace</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/cities/edit/136860" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Willer</td>
								<td><a href="/panel/countries/edit/13/">Франция</a></td>
								<td>Alsace</td>
								<td>Alsace</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/cities/edit/136859" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Oberdorf</td>
								<td><a href="/panel/countries/edit/13/">Франция</a></td>
								<td>Alsace</td>
								<td>Alsace</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/cities/edit/136858" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Grentzingen</td>
								<td><a href="/panel/countries/edit/13/">Франция</a></td>
								<td>Alsace</td>
								<td>Alsace</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/cities/edit/136857" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Henflingen</td>
								<td><a href="/panel/countries/edit/13/">Франция</a></td>
								<td>Alsace</td>
								<td>Alsace</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/cities/edit/136856" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
						</tbody>
					</table>

                    


<ul class="pagination">

  <li class="disabled"><a href="#" onclick="javascript: return false;">«</a></li>


  
    
  <li class="active"><a href="#" onclick="javascript: return false;">1</a></li>
    
  

  
    
  <li><a href="?page=2">2</a></li>
    
  

  
    
  <li><a href="?page=3">3</a></li>
    
  

  
    
  <li><a href="?page=4">4</a></li>
    
  

  
  <li class="disabled"><a href="#" onclick="javascript: return false;">...</a></li>
  

  
    
  <li><a href="?page=12378">12378</a></li>
    
  

  
    
  <li><a href="?page=12379">12379</a></li>
    
  

  
    
  <li><a href="?page=12380">12380</a></li>
    
  

  
    
  <li><a href="?page=12381">12381</a></li>
    
  


  <li><a href="?page=2">»</a></li>

</ul>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
