<section class="content-header">
    <h1>
        Orders
        <small>Orders to send ro</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Orders</li>
    </ol>
</section>

<form id="packing">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">

                <!-- /.box-header -->
                <div class="box-header">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                            <label class="col-sm-1 control-label">Courier:</label>

                            <div class="col-sm-2">
                                 <select class="form-control select2 select2-hidden-accessible" id="id_delivery" name="delivery" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="4">Почта Румынии</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 183px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_delivery-container"><span class="select2-selection__rendered" id="select2-id_delivery-container" title="---------">---------</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>

                            <label class="col-sm-1 control-label">From:</label>

                            <div class="col-sm-2">
                                 <div class="input-group">
                                     <div class="input-group-addon">
                                         <i class="fa fa-calendar"></i>
                                     </div>
                                     <input id="date_start_csv" name="date_start" class="form-control datepicker" data-date-format="yyyy-mm-dd" type="text" value="">
                                 </div>
                            </div>

                            <label class="col-sm-1 control-label">To:</label>

                            <div class="col-sm-2">
                               <div class="input-group" style="float:right;">
                                   <div class="input-group-addon">
                                       <i class="fa fa-calendar"></i>
                                   </div>
                                   <input id="date_end_csv" name="date_end" class="form-control datepicker" data-date-format="yyyy-mm-dd" type="text" value="">
                               </div>
                            </div>


                            <div class="col-sm-2">
                                 <button name="filter" value="filter" type="submit" class="btn btn-block btn-success" style="float:right;">
                                     <span class="glyphicon glyphicon-filter"></span> <b>Courier Info</b>
                                 </button>
                            </div>
                            </div>


                        </div>
                    </div>
                    </div>

                <div class="box-header">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="col-md-5">
                            <div class="btn-group">
                                <button data-url="/panel/packing/ro/print_invoice/?" id="print" type="button" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print Invoce</button>
                            </div>
                            <div class="btn-group">
                                <button data-url="/panel/packing/ro/print_fan/?" id="print_fan" type="button" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print FAN</button>
                            </div>
                        </div>

                        <div class="btn-group">
                            <button data-url="/panel/packing/ro/gen_xls/?" id="dowload_xls_common" name="download_xls" value="download_xls" type="button" class="btn btn-block btn-block-menu btn-success">
                                <span class="glyphicon glyphicon-save"></span> <b>Dowload XLS </b>
                            </button>
                        </div>

                        <div class="btn-group">
                            <button data-url="/panel/packing/ro/gen_csv/?" id="dowload_csv_common" name="dowload_csv_common" value="download_csv" type="button" class="btn btn-block btn-block-menu btn-success">
                                <span class="glyphicon glyphicon-save"></span> <b>Dowload CSV </b>
                            </button>
                        </div>
                </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                        <div class="col-xs-6 col-md-3 text-center">
                            <div style="display:inline;width:90px;height:90px;"><canvas width="90" height="90"></canvas><input type="text" class="knob" value="99" data-width="90" data-height="90" data-fgcolor="#3c8dbc" data-readonly="true" readonly="readonly" style="width: 49px; height: 30px; position: absolute; vertical-align: middle; margin-top: 30px; margin-left: -69px; border: 0px; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 18px; line-height: normal; font-family: Arial; text-align: center; color: rgb(60, 141, 188); padding: 0px; -webkit-appearance: none; background: none;"></div>

                            <div class="knob-label">Total parcel / Sent parcel</div>
                            <div class="knob-label"><b>69473 / 68528</b></div>
                        </div>

                        <div class="col-xs-6 col-md-3 text-center">
                            <div style="display:inline;width:90px;height:90px;"><canvas width="90" height="90"></canvas><input type="text" class="knob" value="2" data-width="90" data-height="90" data-fgcolor="#00c0ef" data-readonly="true" readonly="readonly" style="width: 49px; height: 30px; position: absolute; vertical-align: middle; margin-top: 30px; margin-left: -69px; border: 0px; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 18px; line-height: normal; font-family: Arial; text-align: center; color: rgb(0, 192, 239); padding: 0px; -webkit-appearance: none; background: none;"></div>

                            <div class="knob-label">On the way + waiting</div>
                            <div class="knob-label"><b>1524 / 68528</b></div>
                        </div>

                        <div class="col-xs-6 col-md-3 text-center">
                            <div style="display:inline;width:90px;height:90px;"><canvas width="90" height="90"></canvas><input type="text" class="knob" value="76 %" data-width="90" data-height="90" data-fgcolor="#00a65a" data-readonly="true" readonly="readonly" style="width: 49px; height: 30px; position: absolute; vertical-align: middle; margin-top: 30px; margin-left: -69px; border: 0px; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 18px; line-height: normal; font-family: Arial; text-align: center; color: rgb(0, 166, 90); padding: 0px; -webkit-appearance: none; background: none;"></div>

                            <div class="knob-label">Paid</div>
                            <div class="knob-label"><b>51786 / 68528</b></div>
                        </div>

                        <div class="col-xs-6 col-md-3 text-center">
                            <div style="display:inline;width:90px;height:90px;"><canvas width="90" height="90"></canvas><input type="text" class="knob" value="22 %" data-width="90" data-height="90" data-fgcolor="#f56954" data-readonly="true" readonly="readonly" style="width: 49px; height: 30px; position: absolute; vertical-align: middle; margin-top: 30px; margin-left: -69px; border: 0px; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 18px; line-height: normal; font-family: Arial; text-align: center; color: rgb(245, 105, 84); padding: 0px; -webkit-appearance: none; background: none;"></div>

                            <div class="knob-label">Returned</div>
                            <div class="knob-label"><b>15218 / 68528</b></div>
                        </div>
                    </div>

                <hr>

                <div class="box-body table-responsive">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
                                <th>
                                    <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" id="check_all" name="check_all" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                </th>
								<th style="width:100px;"><input value="" name="id" class="form-control" type="text"></th>
								<th></th>
								<th><input class="form-control" id="id_trackid" maxlength="128" name="trackid" type="text"></th>
                                <th></th>
								<th><input class="form-control" id="id_name" maxlength="1024" name="name" type="text"></th>
								<th></th>

								<th style="width:80px;"></th>

								<th></th>
                                <th></th>
                                <th></th>

                                <th style="width:140px">
                                    <select name="status" id="status" style="width:140px">
                                        <option value="">---------</option>
                                        <option value="20">заказ принят и передан на отправку</option>
                                        <option value="21">заказ отправлен</option>
                                        <option value="22">заказ ожидает на почте</option>
                                        <option value="23">заказ оплачен</option>
                                        <option value="24">возврат заказа</option>
                                    </select>
                                </th>
								<th style="min-width:100px; max-width:100px;"></th>
                                <th style="min-width:100px; max-width:100px;"></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
								<th>Ch</th>
								<th>ID</th>
								<th>Goods</th>
								<th>TrackId</th>
                                <th>Invoice</th>
								<th>Name</th>

								<th>Dep.</th>
								<th>Preț</th>
                                <th>Delivery_price</th>
                                <th>Without tax</th>

								<th>Oraș</th>
                                <th>Status</th>
								<th>Created</th>
								<th>Send</th>
                                <th></th>
							</tr>
						</thead>
						<tbody>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="101185" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>101185</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-101185" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-101185">6079528450218</span>
								</td>
                                <td>None</td>
								<td>MIHAI </td>

								<td> </td>
								<td>160,0 / 1</td>
                                <td>5,5</td>
                                <td>129,03</td>

								<td>Iasi</td>
                                <td>Paid</td>
								<td>
									2015-03-20<br>
									16:15
								</td>
								<!--<td>-->
									<!--2015-03-21<br>-->
									<!--14:04-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/101185" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="100705" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>100705</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-100705" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-100705">6079528450050</span>
								</td>
                                <td>None</td>
								<td>marius</td>

								<td> </td>
								<td>213,0 / 2</td>
                                <td>5,5</td>
                                <td>171,77</td>

								<td>Craiova</td>
                                <td>Paid</td>
								<td>
									2015-03-20<br>
									1:35
								</td>
								<!--<td>-->
									<!--2015-03-23<br>-->
									<!--14:07-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/100705" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="100691" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>100691</td>
								<td>Electricity Saving Box</td>
								<td>
									<div id="spinner-100691" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-100691">6079528450064</span>
								</td>
                                <td>None</td>
								<td>DORU SITARU</td>

								<td> </td>
								<td>200,0 / 1</td>
                                <td>5,5</td>
                                <td>161,29</td>

								<td>Odorheiu Secuiesc</td>
                                <td>Return</td>
								<td>
									2015-03-20<br>
									0:45
								</td>
								<!--<td>-->
									<!--2015-04-29<br>-->
									<!--17:29-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/100691" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="100516" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>100516</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-100516" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-100516">6079528450030</span>
								</td>
                                <td>None</td>
								<td>galgoczi daniel</td>

								<td> </td>
								<td>213,0 / 2</td>
                                <td>5,5</td>
                                <td>171,77</td>

								<td>Timisoara</td>
                                <td>Paid</td>
								<td>
									2015-03-19<br>
									20:55
								</td>
								<!--<td>-->
									<!--2015-03-23<br>-->
									<!--20:21-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/100516" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="100165" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>100165</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-100165" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-100165">6078528450159</span>
								</td>
                                <td>None</td>
								<td>nicu</td>

								<td> </td>
								<td>213,0 / 2</td>
                                <td>5,5</td>
                                <td>171,77</td>

								<td>Campulung Moldovenesc</td>
                                <td>Paid</td>
								<td>
									2015-03-19<br>
									14:30
								</td>
								<!--<td>-->
									<!--2015-03-23<br>-->
									<!--14:06-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/100165" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="99464" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>99464</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-99464" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-99464">6078528450116</span>
								</td>
                                <td>None</td>
								<td>khaled dezine</td>

								<td> </td>
								<td>213,0 / 2</td>
                                <td>5,5</td>
                                <td>171,77</td>

								<td>Constanta</td>
                                <td>Paid</td>
								<td>
									2015-03-18<br>
									18:50
								</td>
								<!--<td>-->
									<!--2015-03-20<br>-->
									<!--20:34-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/99464" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="98992" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>98992</td>
								<td>Electricity Saving Box</td>
								<td>
									<div id="spinner-98992" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-98992">6077528450036</span>
								</td>
                                <td>None</td>
								<td>sima mihaela*</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>289,0 / 2</td>
                                <td>5,5</td>
                                <td>233,06</td>

								<td>Ghimpati</td>
                                <td>Return</td>
								<td>
									2015-03-18<br>
									7:55
								</td>
								<!--<td>-->
									<!--2015-04-29<br>-->
									<!--17:29-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/98992" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="110279" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>110279</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-110279" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-110279">6090528450025</span>
								</td>
                                <td>None</td>
								<td>Timis vasile</td>

								<td> </td>
								<td>219,0 / 2</td>
                                <td>5,5</td>
                                <td>176,61</td>

								<td>Borsa</td>
                                <td>At department</td>
								<td>
									2015-03-31<br>
									2:50
								</td>
								<!--<td>-->
									<!--2015-10-01<br>-->
									<!--0:21-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/110279" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="98718" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>98718</td>
								<td>Electricity Saving Box</td>
								<td>
									<div id="spinner-98718" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-98718">6077528450010</span>
								</td>
                                <td>None</td>
								<td>antal atila</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>289,0 / 2</td>
                                <td>5,5</td>
                                <td>233,06</td>

								<td>Miercurea-Ciuc</td>
                                <td>Paid</td>
								<td>
									2015-03-17<br>
									20:00
								</td>
								<!--<td>-->
									<!--2015-03-24<br>-->
									<!--13:09-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/98718" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="98414" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>98414</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-98414" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-98414">6076528450327</span>
								</td>
                                <td>None</td>
								<td>Ciprian</td>

								<td> </td>
								<td>149,0 / 1</td>
                                <td>5,5</td>
                                <td>120,16</td>

								<td>Selimbar</td>
                                <td>Paid</td>
								<td>
									2015-03-17<br>
									12:40
								</td>
								<!--<td>-->
									<!--2015-03-18<br>-->
									<!--23:21-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/98414" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="97638" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>97638</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-97638" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-97638">6075528450195</span>
								</td>
                                <td>033292</td>
								<td>lostun dorin</td>

								<td> </td>
								<td>213,0 / 2</td>
                                <td>5,5</td>
                                <td>171,77</td>

								<td>Vatra Dornei</td>
                                <td>Paid</td>
								<td>
									2015-03-16<br>
									8:45
								</td>
								<!--<td>-->
									<!--2015-03-18<br>-->
									<!--14:06-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/97638" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="97244" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>97244</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-97244" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-97244">6075528450098</span>
								</td>
                                <td>None</td>
								<td> Ujica Avelin</td>

								<td> </td>
								<td>213,0 / 2</td>
                                <td>5,5</td>
                                <td>171,77</td>

								<td>Brasov</td>
                                <td>Paid</td>
								<td>
									2015-03-15<br>
									18:25
								</td>
								<!--<td>-->
									<!--2015-03-17<br>-->
									<!--22:28-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/97244" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="96419" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>96419</td>
								<td>Electricity Saving Box</td>
								<td>
									<div id="spinner-96419" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-96419">6076528450113</span>
								</td>
                                <td>None</td>
								<td>Costache Gelu</td>

								<td> </td>
								<td>200,0 / 1</td>
                                <td>5,5</td>
                                <td>161,29</td>

								<td>Slobozia</td>
                                <td>Paid</td>
								<td>
									2015-03-14<br>
									18:20
								</td>
								<!--<td>-->
									<!--2015-03-18<br>-->
									<!--23:20-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/96419" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="271570" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>271570</td>
								<td>Breast Cream</td>
								<td>
									<div id="spinner-271570" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-271570">6265549360062</span>
								</td>
                                <td>014071</td>
								<td>Stefan Melania Oana</td>

								<td> </td>
								<td>175,0 / 1</td>
                                <td>5,5</td>
                                <td>141,13</td>

								<td>Caransebes</td>
                                <td>Paid</td>
								<td>
									2015-09-21<br>
									20:40
								</td>
								<!--<td>-->
									<!--2015-09-24<br>-->
									<!--0:16-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/271570" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="95167" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>95167</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-95167" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-95167">6072528450084</span>
								</td>
                                <td>None</td>
								<td>ANTAL VILHELM</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>213,0 / 2</td>
                                <td>5,5</td>
                                <td>171,77</td>

								<td>Miercurea-Ciuc</td>
                                <td>Paid</td>
								<td>
									2015-03-13<br>
									7:45
								</td>
								<!--<td>-->
									<!--2015-03-23<br>-->
									<!--20:11-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/95167" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="90705" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>90705</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-90705" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-90705">6065528450125</span>
								</td>
                                <td>None</td>
								<td>Popa Alin</td>

								<td> </td>
								<td>213,0 / 2</td>
                                <td>5,5</td>
                                <td>171,77</td>

								<td>Bucuresti</td>
                                <td>Paid</td>
								<td>
									2015-03-06<br>
									14:35
								</td>
								<!--<td>-->
									<!--2015-03-09<br>-->
									<!--16:39-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/90705" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="110598" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>110598</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-110598" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-110598">6090528450095</span>
								</td>
                                <td>None</td>
								<td>Nicolae</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>160,0 / 1</td>
                                <td>5,5</td>
                                <td>129,03</td>

								<td>Berbesti</td>
                                <td>At department</td>
								<td>
									2015-03-31<br>
									12:55
								</td>
								<!--<td>-->
									<!--2015-10-01<br>-->
									<!--0:15-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/110598" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="88988" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>88988</td>
								<td>Electricity Saving Box</td>
								<td>
									<div id="spinner-88988" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-88988">6063528450016</span>
								</td>
                                <td>None</td>
								<td>Creanga Mirela</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>200,0 / 1</td>
                                <td>5,5</td>
                                <td>161,29</td>

								<td>Targu Neamt</td>
                                <td>At department</td>
								<td>
									2015-03-03<br>
									21:04
								</td>
								<!--<td>-->
									<!--2015-10-01<br>-->
									<!--0:15-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/88988" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="87561" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>87561</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-87561" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-87561">6064528450124</span>
								</td>
                                <td>None</td>
								<td>cioaca adrian</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>213,0 / 2</td>
                                <td>5,5</td>
                                <td>171,77</td>

								<td>Valea Bujorului</td>
                                <td>At department</td>
								<td>
									2015-03-01<br>
									12:20
								</td>
								<!--<td>-->
									<!--2015-10-01<br>-->
									<!--0:15-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/87561" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="90024" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>90024</td>
								<td>Electricity Saving Box</td>
								<td>
									<div id="spinner-90024" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-90024">6064528450091</span>
								</td>
                                <td>None</td>
								<td>POP MARIA</td>

								<td> </td>
								<td>289,0 / 2</td>
                                <td>5,5</td>
                                <td>233,06</td>

								<td>Bistrita</td>
                                <td>Paid</td>
								<td>
									2015-03-05<br>
									11:45
								</td>
								<!--<td>-->
									<!--2015-03-09<br>-->
									<!--10:03-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/90024" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="89974" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>89974</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-89974" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-89974">6064528450162</span>
								</td>
                                <td>None</td>
								<td>Trandafir Trandfir</td>

								<td> </td>
								<td>149,0 / 1</td>
                                <td>5,5</td>
                                <td>120,16</td>

								<td>Bucuresti</td>
                                <td>Paid</td>
								<td>
									2015-03-05<br>
									10:35
								</td>
								<!--<td>-->
									<!--2015-03-06<br>-->
									<!--14:05-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/89974" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="96129" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>96129</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-96129" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-96129">6076528450070</span>
								</td>
                                <td>None</td>
								<td>didu crenguta</td>

								<td> </td>
								<td>149,0 / 1</td>
                                <td>5,5</td>
                                <td>120,16</td>

								<td>Bucuresti</td>
                                <td>Paid</td>
								<td>
									2015-03-14<br>
									10:05
								</td>
								<!--<td>-->
									<!--2015-03-18<br>-->
									<!--23:20-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/96129" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="89583" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>89583</td>
								<td>Electricity Saving Box</td>
								<td>
									<div id="spinner-89583" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-89583">6064528450029</span>
								</td>
                                <td>None</td>
								<td>draganescu ionut</td>

								<td> </td>
								<td>200,0 / 1</td>
                                <td>5,5</td>
                                <td>161,29</td>

								<td>Bucuresti</td>
                                <td>Return</td>
								<td>
									2015-03-04<br>
									18:34
								</td>
								<!--<td>-->
									<!--2015-04-29<br>-->
									<!--17:26-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/89583" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="89358" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>89358</td>
								<td>Electricity Saving Box</td>
								<td>
									<div id="spinner-89358" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-89358">6063528450142</span>
								</td>
                                <td>None</td>
								<td>MILITARU MARINICA</td>

								<td> </td>
								<td>200,0 / 1</td>
                                <td>5,5</td>
                                <td>161,29</td>

								<td>Pitesti</td>
                                <td>Return</td>
								<td>
									2015-03-04<br>
									12:50
								</td>
								<!--<td>-->
									<!--2015-03-05<br>-->
									<!--18:08-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/89358" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="89252" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>89252</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-89252" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-89252">6063528450131</span>
								</td>
                                <td>None</td>
								<td>bressel ioan</td>

								<td> </td>
								<td>213,0 / 2</td>
                                <td>5,5</td>
                                <td>171,77</td>

								<td>Halmeu</td>
                                <td>Paid</td>
								<td>
									2015-03-04<br>
									9:55
								</td>
								<!--<td>-->
									<!--2015-03-06<br>-->
									<!--21:25-->
								<!--</td>-->
								<td>
									<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/89252" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
						</tbody>
					</table>

                    


<ul class="pagination">

  <li class="disabled"><a href="#" onclick="javascript: return false;">«</a></li>


  
    
  <li class="active"><a href="#" onclick="javascript: return false;">1</a></li>
    
  

  
    
  <li><a href="?page=2">2</a></li>
    
  

  
    
  <li><a href="?page=3">3</a></li>
    
  

  
    
  <li><a href="?page=4">4</a></li>
    
  

  
  <li class="disabled"><a href="#" onclick="javascript: return false;">...</a></li>
  

  
    
  <li><a href="?page=2776">2776</a></li>
    
  

  
    
  <li><a href="?page=2777">2777</a></li>
    
  

  
    
  <li><a href="?page=2778">2778</a></li>
    
  

  
    
  <li><a href="?page=2779">2779</a></li>
    
  


  <li><a href="?page=2">»</a></li>

</ul>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
<script>
 $(function() {
        $(".knob").knob();
    });
</script>
