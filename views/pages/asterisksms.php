<section class="content-header">
    <h1>
	    AsteriskSMS
        <small>AsteriskSMS</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">AsteriskSMS</li>
    </ol>
</section>

<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">



                </div>
                <div class="box-body table-responsive no-padding">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
								<th><select class="form-control select2 select2-hidden-accessible" id="id_country" name="country" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="3">Румыния</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 91px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_country-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
								<th><input class="form-control" id="id_order" name="order" type="text"></th>
								<th><input class="form-control" id="id_phone" maxlength="512" name="phone" type="text"></th>
								<th><select class="form-control select2 select2-hidden-accessible" id="id_type" name="type" tabindex="-1" aria-hidden="true">
<option value="1">Outcoming</option>
<option value="2">Incoming</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 102px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_type-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
								<th><input class="form-control" id="id_simcard" name="simcard" type="text"></th>
								<th><select class="form-control select2 select2-hidden-accessible" id="id_message_type" name="message_type" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">------</option>
<option value="1">message</option>
<option value="2">recall</option>
<option value="3">shipped</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 90px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_message_type-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
								<th><select class="form-control select2 select2-hidden-accessible" id="id_status" name="status" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">------</option>
<option value="1">success</option>
<option value="2">faild</option>
<option value="3">sending</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 83px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_status-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
								<th></th>
								<th></th>


                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
								<th>Tară</th>
                                <th>Ordine</th>
								<th>Telefon</th>
								<th>Type</th>
								<th>SimCard</th>
								<th>Message Type</th>
								<th>Status</th>
								<th>Message</th>
								<th>Created</th>


                                <th style="width: 116px; min-width:116px;"></th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>

                </div>
                <div class="box-footer">
                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
<!-- /.content -->