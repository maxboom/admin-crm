<section class="content-header">
    <h1>
        Monitoring
        <small>monitoring table</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Monitoring</li>
    </ol>
</section>
<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
				<!-- /.box-header -->
                <div class="box-header">
                </div>
                <div class="box-body table-responsive ">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">

								<th><input class="form-control" id="id_country" maxlength="128" name="country" type="text"></th>
								<th><input class="form-control" id="id_code" maxlength="128" name="code" type="text"></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">

								<th>Tară</th>
								<th>Code</th>
                                <th style="width: 116px; min-width:116px;">
                                </th>
							</tr>
						</thead>
						<tbody>
						
							<tr>

								<td>Румыния</td>
								<td>RO</td>
                                <td>
                                    <div class="btn-group pull-right">

                                        <a href="/panel/monitoring/3" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>

                                    </div>
                                </td>
							</tr>
						
						</tbody>
					</table>

                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
