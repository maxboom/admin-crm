<section class="content-header">
    <h1>
	    ZipCode
        <small>ZipCode</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">zipcode</li>
    </ol>
</section>

<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">
                    <a href="/panel/zipcodes/create/" class="btn btn-sm btn-primary">
                        <span class="glyphicon glyphicon-new-window"></span> <b>Add item </b>
                    </a>
                </div>
                <div class="box-body table-responsive no-padding">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
								<th><select class="form-control select2 select2-hidden-accessible" id="id_country" name="country" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="3">Румыния</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 241px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_country-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
                                <th>
                                    <select name="state" id="id_state" class="form-control">
                                        <option value="">---------</option>
                                            
                                    </select>
                                </th>
								<th><select class="form-control select2 select2-hidden-accessible" id="id_city" name="city" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 165px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_city-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
								<th><input class="form-control" id="id_code" maxlength="256" name="code" type="text"></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
								<th>Tară</th>
                                <th>State</th>
								<th>Oraș</th>
								<th>Zipcode</th>
                                <th style="width: 116px; min-width:116px;">
                                </th>
							</tr>
						</thead>
						<tbody>
						
							<tr>
								<td>Италия</td>
                                <td>ROMA</td>
								<td>Valcanneto</td>
								<td>00052</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/zipcodes/edit/358530" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Франция</td>
                                <td>Alsace</td>
								<td>Heimsbrunn</td>
								<td>68990</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/zipcodes/edit/358529" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Франция</td>
                                <td>Alsace</td>
								<td>Galfingue</td>
								<td>68990</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/zipcodes/edit/358528" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Франция</td>
                                <td>Alsace</td>
								<td>Beblenheim</td>
								<td>68980</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/zipcodes/edit/358527" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Франция</td>
                                <td>Alsace</td>
								<td>Guémar</td>
								<td>68970</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/zipcodes/edit/358526" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Франция</td>
                                <td>Alsace</td>
								<td>Illhaeusern</td>
								<td>68970</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/zipcodes/edit/358525" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Франция</td>
                                <td>Alsace</td>
								<td>Mulhouse</td>
								<td>68968CEDEX9</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/zipcodes/edit/358524" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Франция</td>
                                <td>Alsace</td>
								<td>Willer</td>
								<td>68960</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/zipcodes/edit/358523" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Франция</td>
                                <td>Alsace</td>
								<td>Oberdorf</td>
								<td>68960</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/zipcodes/edit/358522" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    </div>
                                </td>
							</tr>
						
							<tr>
								<td>Франция</td>
                                <td>Alsace</td>
								<td>Grentzingen</td>
								<td>68960</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/zipcodes/edit/358521" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    </div>
                                </td>
							</tr>
						
						</tbody>
					</table>

                </div>
                <div class="box-footer">
                    


<ul class="pagination">

  <li class="disabled"><a href="#" onclick="javascript: return false;">«</a></li>


  
    
  <li class="active"><a href="#" onclick="javascript: return false;">1</a></li>
    
  

  
    
  <li><a href="?page=2">2</a></li>
    
  

  
    
  <li><a href="?page=3">3</a></li>
    
  

  
    
  <li><a href="?page=4">4</a></li>
    
  

  
  <li class="disabled"><a href="#" onclick="javascript: return false;">...</a></li>
  

  
    
  <li><a href="?page=35850">35850</a></li>
    
  

  
    
  <li><a href="?page=35851">35851</a></li>
    
  

  
    
  <li><a href="?page=35852">35852</a></li>
    
  

  
    
  <li><a href="?page=35853">35853</a></li>
    
  


  <li><a href="?page=2">»</a></li>

</ul>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
