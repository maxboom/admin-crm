<section class="content-header">
        <h1>
            Courier Statistic
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Courier statistic</li>
        </ol>
    </section>

<!--main content-->
<!--form block-->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            
            <div class="callout callout-warning">
                <div class="row">
                    <h4 class="col-xs-12">Unconfirmed transactions</h4>
                    
                        
                    
                        
                    
                        
                    
                        
                        <div class="col-xs-4">
                            <dl>
                                <dt>April 2015</dt>
                                <dd>Unconfirmed payments: 3860</dd>
                                <dd>Unconfirmed returns: 1090</dd>
                            </dl>
                        </div>
                        
                    
                        
                        <div class="col-xs-4">
                            <dl>
                                <dt>May 2015</dt>
                                <dd>Unconfirmed payments: 4639</dd>
                                <dd>Unconfirmed returns: 1321</dd>
                            </dl>
                        </div>
                        
                    
                        
                        <div class="col-xs-4">
                            <dl>
                                <dt>June 2015</dt>
                                <dd>Unconfirmed payments: 2547</dd>
                                <dd>Unconfirmed returns: 723</dd>
                            </dl>
                        </div>
                        
                    
                        
                        <div class="col-xs-4">
                            <dl>
                                <dt>July 2015</dt>
                                <dd>Unconfirmed payments: 2645</dd>
                                <dd>Unconfirmed returns: 789</dd>
                            </dl>
                        </div>
                        
                    
                        
                        <div class="col-xs-4">
                            <dl>
                                <dt>August 2015</dt>
                                <dd>Unconfirmed payments: 3047</dd>
                                <dd>Unconfirmed returns: 846</dd>
                            </dl>
                        </div>
                        
                    
                        
                        <div class="col-xs-4">
                            <dl>
                                <dt>September 2015</dt>
                                <dd>Unconfirmed payments: 4084</dd>
                                <dd>Unconfirmed returns: 1291</dd>
                            </dl>
                        </div>
                        
                    
                        
                        <div class="col-xs-4">
                            <dl>
                                <dt>October 2015</dt>
                                <dd>Unconfirmed payments: 4114</dd>
                                <dd>Unconfirmed returns: 1438</dd>
                            </dl>
                        </div>
                        
                    
                        
                        <div class="col-xs-4">
                            <dl>
                                <dt>November 2015</dt>
                                <dd>Unconfirmed payments: 4264</dd>
                                <dd>Unconfirmed returns: 1574</dd>
                            </dl>
                        </div>
                        
                    
                        
                        <div class="col-xs-4">
                            <dl>
                                <dt>December 2015</dt>
                                <dd>Unconfirmed payments: 4274</dd>
                                <dd>Unconfirmed returns: 1713</dd>
                            </dl>
                        </div>
                        
                    
                        
                        <div class="col-xs-4">
                            <dl>
                                <dt>January 2016</dt>
                                <dd>Unconfirmed payments: 6073</dd>
                                <dd>Unconfirmed returns: 1756</dd>
                            </dl>
                        </div>
                        
                    
                        
                        <div class="col-xs-4">
                            <dl>
                                <dt>February 2016</dt>
                                <dd>Unconfirmed payments: 4295</dd>
                                <dd>Unconfirmed returns: 591</dd>
                            </dl>
                        </div>
                        
                    
                </div>
            </div>
            
        </div>
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="" method="GET" role="form" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label">Courier:</label>
                                    <div class="col-sm-2">
                                        <select class="form-control select2 select2-hidden-accessible" id="id_courier" name="courier" tabindex="-1" aria-hidden="true">
<option value="4">Почта Румынии</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 183px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_courier-container"><span class="select2-selection__rendered" id="select2-id_courier-container" title="Почта Румынии">Почта Румынии</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                    </div>

                                    <label class="col-sm-1 control-label">Month:</label>
                                    <div class="col-sm-2">
                                        <select class="form-control select2 select2-hidden-accessible" id="id_month" name="month" tabindex="-1" aria-hidden="true">
<option value="1">Январь</option>
<option value="2" selected="selected">Февраль</option>
<option value="3">Март</option>
<option value="4">Апрель</option>
<option value="5">Май</option>
<option value="6">Июнь</option>
<option value="7">Июль</option>
<option value="8">Август</option>
<option value="9">Сентябрь</option>
<option value="10">Октябрь</option>
<option value="11">Ноябрь</option>
<option value="12">Декабрь</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 183px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_month-container"><span class="select2-selection__rendered" id="select2-id_month-container" title="Февраль">Февраль</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                    </div>

                                    <label class="col-sm-1 control-label">Year:</label>
                                    <div class="col-sm-2">
                                        <select class="form-control select2 select2-hidden-accessible" id="id_year" name="year" tabindex="-1" aria-hidden="true">
<option value="2016" selected="selected">2016</option>
<option value="2015">2015</option>
<option value="2014">2014</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 183px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_year-container"><span class="select2-selection__rendered" id="select2-id_year-container" title="2016">2016</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                    </div>

                                    <div class="col-sm-2">
                                        <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                            <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>


                        <!--boxes-->
                       <div class="row">
                        <div class="box-body">
                           <div class="col-lg-3 col-xs-5">
                                <!-- small box blue -->
                                <div class="small-box bg-aqua">
                                    <div class="inner">
                                        <h3>0<font size="4"> / 0</font></h3>
                                        <p>Sent parcels / Total parcels</p>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-shopping-cart"></i>
                                    </div>
                                    <a href="#" class="small-box-footer">
                                        More info <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                            </div>
                           <div class="col-lg-3 col-xs-5">
                                <!-- small box green -->
                                <div class="small-box bg-green">
                                    <div class="inner">
                                        <h3>0%<font size="4">
                                            / 0 %</font></h3>
                                        <p>Confirmed paid / Total paid </p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-stats-bars"></i>
                                    </div>
                                    <a href="#" class="small-box-footer">
                                        More info <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                           </div>

                           <div class="col-lg-3 col-xs-5">
                                <!-- small box orange -->
                                <div class="small-box bg-yellow">
                                    <div class="inner">
                                        <h3><font size="6">0</font>
                                            <font size="3">
                                            / 0</font></h3>
                                        <p>Confirmed total price / Total price</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-person-add"></i>
                                    </div>
                                    <a href="#" class="small-box-footer">
                                        More info <i class="fa fa-arrow-circle-right"></i>
                                    </a>
                                </div>
                           </div>

                           <div class="col-lg-3 col-xs-5">
                               <!-- small box red -->
                               <div class="small-box bg-red">
                                   <div class="inner">
                                       <h3>0%</h3>
                                       <p>Divergence</p>
                                   </div>
                                   <div class="icon">
                                       <i class="ion ion-pie-graph"></i>
                                   </div>
                                   <a href="#" class="small-box-footer">
                                       More info <i class="fa fa-arrow-circle-right"></i>
                                   </a>
                               </div>
                           </div>

                            </div>
                        </div>
                        <!--boxes end-->
<hr>
                            <div class="row">
                                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal" role="form">
                                    
                                    <div class="col-md-4" style="margin-left: 2%;">
                                        <div class="body-box">
                                            <p align="left"><label class="control-label">Update GS statuses:</label></p>

                                                    <div class="input-group margin">
                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                Action
                                                                <span class="fa fa-caret-down"></span></button>
                                                            <ul class="dropdown-menu">
                                                                <li><a><button class="btn btn-file" style="background:none;">Upload file<input id="id_update_status" name="update_status" type="file"></button></a></li>
                                                                <li><a><button class="btn" name="upload_status" style="background:none;">Generate report</button></a></li>
                                                            </ul>
                                                        </div>
                                                <!-- /btn-group -->
                                                        <input type="text" class="form-control" readonly="" style="width: 80%;">
                                                    </div>
                                            </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="body-box">
                                            <p align="left"><label class="control-label">Upload payment:</label></p>
                                                <div class="input-group margin">
                                                    <div class="input-group-btn">
                                                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                            Action
                                                            <span class="fa fa-caret-down"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <li><a><button class="btn btn-file" style="background:none;">Upload file<input id="id_update_payment" name="update_payment" type="file"></button></a></li>
                                                            <li><a><button class="btn" name="upload_payment" style="background:none;">Generate report</button></a></li>
                                                        </ul>
                                                    </div>
                                            <!-- /btn-group -->
                                                    <input type="text" class="form-control" readonly="" style="width: 80%;">
                                                </div>

                                            </div>
                                    </div>
                                </form>
                                <!--<div class="col-md-3">-->
                                    <!--<div class="body-box">-->
                                        <!--<p><label class="control-label">Returned trackers:</label></p>-->

                                                <!--<div class="input-group margin">-->
                                                    <!--<div class="input-group-btn">-->
                                                        <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">-->
                                                            <!--Enter trackers-->
                                                        <!--</button>-->
                                                    <!--</div>-->
                                                <!--</div>-->
                                        <!--<form action='' method="post" class="form-horizontal" role="form">-->
                                            <!---->
                                        <!--<div class="modal fade" id="myModal" role="dialog">-->
                                            <!--<div class="modal-dialog">-->
                                                <!--<div class="modal-content">-->
                                                    <!--<div class="modal-header">-->
                                                        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
                                                            <!--<span aria-hidden="true">×</span></button>-->
                                                        <!--<h4 class="modal-title">Default Modal</h4>-->
                                                    <!--</div>-->
                                                    <!--<div class="modal-body">-->
                                                            <!--<p></p>-->
                                                    <!--</div>-->
                                                    <!--<div class="modal-footer">-->
                                                        <!--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>-->
                                                        <!--<input type="submit" class="btn btn-primary" name="confirm" value="Save changes"/>-->
                                                    <!--</div>-->
                                                <!--</div>-->
                                            <!--</div>-->
                                        <!--</div>-->
                                        <!--</form>-->

                                    <!--</div>-->
                                <!--</div>-->

                                <!--<div class="col-md-3">-->
                                    <!--<div class="body-box">-->
                                        <!--<p align="left"><label class="control-label">Last modified:</label></p>-->
                                        <!--<p><label>0</label></p>-->
                                    <!--</div>-->
                                <!--</div>-->
                            </div>

<!--statictics table-->
<div class="box-header">
    <h3 class="box-title">Statistic</h3>
</div>

<div class="box-body table-responsive no-padding">
    <table class="table table-hover" style="font-size: 11px;">
        <tbody><tr>
            <th rowspan="2">Date</th>
            <th rowspan="2">Newly<br>created<br>parcels</th>
            <th rowspan="2">Total number<br>of parcels<br>to send</th>
            <th colspan="3">Sent parcels</th>
            <th colspan="3">Paid parcels</th>
            <th colspan="3">Returned parcels</th>
        </tr>

        <tr>
            <th>Sent</th>
            <th>Other date</th>
            <th>Delayed</th>

            <th>Total</th>
            <th style="max-width:40px; min-width: 40px;">Confirmed</th>
            <th>Divergence</th>

            <th>Total</th>
            <th style="max-width:40px; min-width: 40px;">Confirmed</th>
            <th>Divergence</th>
        </tr>

        
            <tr>
                
                <!--дата-->
                    <td>2016-02-01</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-02</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-03</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-04</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-05</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-06</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-07</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-08</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-09</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-10</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-11</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-12</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-13</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-14</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-15</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-16</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-17</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-18</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-19</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-20</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-21</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                <!--дата-->
                    <td>2016-02-22</td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
            <tr>
                
                    <td><strong>Summary</strong></td>
                
                <!--количество заказов-->
                <td>0</td>
                <td>0</td>

                <!--отправлено-->
                <td>0 (0%)</td>

                <!--другая дата-->
                <td>0</td>

                <!--ожидают-->
                <td>0</td>

                <td>0 (0%)</td>
                <td style="background-color: #E0E0E0;">0 (0%)</td>
                <td>0 (0%)</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <!--<td> (%)</td>-->
                <!--<td style="background-color: #E0E0E0;"> (%)</td>-->
                <!--<td> (%)</td>-->
            </tr>
        
    </tbody></table>
</div>
    </div></div></div>
</div></section>
<!--end of statistic table-->
<!--end of main-->
<script>
  $(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', label);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }

    });
});
</script>