<section class="content-header">
    <h1>
        Orders
        <small>Orders list</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="http://admin-crm.com/panel/orders/#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Orders</li>
    </ol>
</section>

<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">
                    <a class="btn btn-default" href="http://admin-crm.com/panel/edit_order/list_countries/">Add order</a>



                </div>
                <div class="box-body table-responsive">
                    <table class="table table-hover table-vcenter" id="example2">
                        <thead>
                            <tr role="row">
                                <th style="width:100px;"><input class="form-control" id="id_id" name="id" type="text"></th>
                                <th style="width:160px;"><select class="form-control select2 select2-hidden-accessible" id="id_status" name="status" tabindex="-1" aria-hidden="true">
    <option value="" selected="selected">---------</option>
    <option value="0">Pending</option>
    <option value="11">Busy</option>
    <option value="12">Denied</option>
    <option value="13">Duplicate</option>
    <option value="14">Incorrect</option>
    <option value="20">Accepted</option>
    <option value="21">Shipped</option>
    <option value="22">At department</option>
    <option value="23">Paid</option>
    <option value="24">Return</option>
    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 144px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_status-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
                                    <th style="width:120px;"><select class="form-control select2 select2-hidden-accessible" id="id_country" name="country" tabindex="-1" aria-hidden="true">
    <option value="" selected="selected">---------</option>
    <option value="3">Румыния</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 104px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_country-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
                                <th><input class="form-control" id="id_phone" maxlength="512" name="phone" type="text"></th>
                                <th><input class="form-control" id="id_name" maxlength="1024" name="name" type="text"></th>
                                <th></th>
                                <th></th>
                                <th></th>


                                <th><input class="form-control" id="id_trackid" maxlength="128" name="trackid" type="text"></th>
                                <th><div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input id="id_processing" name="processing" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></th>





                                <th style="width:110px;"></th>
                                <th style="width:110px;"></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
                            </tr>
                            <tr role="row">
                                <th>ID</th>
                                <th>Status</th>
                                <th>Tară</th>
                                <th>Telefon</th>
                                <th>Name</th>
                                <th>Goods</th>
                                <th>Preț</th>
                                <th>Calls</th>


                                <th>TrackId</th>
                                <th>Procc</th>





                                <th>Created</th>
                                <th>Modified</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php
              foreach ($orders as $key => $value):
            ?>
                    <tr>
                        <td><?=$value['id']?></td>
                        <td><?=$value['status']?></td>
                        <td><?=$value['country']?></td> 
                        <td><?=$value['phone']?></td>
                        <td><?=$value['name']?></td>
                        <td><?=$value['goods']?></td>
                        <td><?=$value['pret']?></td> 
                        <td><?=$value['calls']?></td>
                        <td><?=$value['track_id']?></td>
                        <td><?=$value['procc']?></td> 
                        <td><?=$value['created']?></td>
                        <td><?=$value['modified']?></td>       
                        <td>

                        <div class="btn-group pull-right">
                                        
                            <a href="/page?id=<?=$value['id']?>" data-id="" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        
                        </div>
                      </td>
                    </tr>

                  <? endforeach; ?>

                        
                        </tbody>
                    </table>

                

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
<!-- /.content -->


<!-- /.content-wrapper -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

<script>

    //   $(function () {
    //     $("#example1").DataTable();
    //     $('#example2').DataTable({
    //       "paging": true,
    //       "lengthChange": true,
    //       "searching": true,
    //       "ordering": true,
    //       "info": true,
    //       "autoWidth": false
    //     });
    //   });

   

    // $(document).ready(function() {
    //   $('table#example2').columnFilters({alternateRowClassNames:['rowa','rowb'], excludeColumns:[3,10]});
    // });
</script>