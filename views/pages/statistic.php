
<section class="content-header">
    <h1>
        Statistic
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Statistic</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <form action="" method="GET" class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group ">
                            <label for="Country" class="col-sm-2 control-label">Tară:</label>
                            <div class="col-sm-5">
                                <select multiple="multiple" class="form-control" id="id_country" name="country">
<option value="3">Румыния</option>
</select>
                                
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="Goods" class="col-sm-2 control-label">Goods:</label>
                            <div class="col-sm-5">
                                <select multiple="multiple" class="form-control" id="id_goods" name="goods">
<option value="18">Electricity Saving Box</option>
<option value="22">Toe Fix</option>
<option value="21">Fito grow</option>
<option value="13">XXL Power Life</option>
<option value="5">Teeth Whitening Pen</option>
<option value="17">Семена Чиа</option>
<option value="16">Носки SOSU</option>
<option value="12">Возбудитель Silver Fox</option>
<option value="7">Магниты от курения ZeroSmoke</option>
<option value="10">«White Light» - набор для отбеливания зубов</option>
<option value="9">Имбирный кофе</option>
<option value="11">Крем для увеличения груди Bust Cream Salon Spa</option>
<option value="6">Энергетические браслеты PowerBalance</option>
<option value="4">Клипса антихрап</option>
<option value="2">Whitelight 3d White</option>
<option value="19">Мужской стимулятор Brutalin</option>
<option value="26">Акулий жир</option>
<option value="25">Гриб Рейша</option>
<option value="24">Шоко Слим</option>
<option value="23">Молот Тора</option>
<option value="15">Жидкий каштан (Гуарана)</option>
<option value="27">Мужские капли Bigzilla</option>
<option value="31">Alcachofa</option>
<option value="36">Годжи крем</option>
<option value="29">Forskolin</option>
<option value="28">Spirulina</option>
<option value="32">Garcinia</option>
<option value="33">Гранатовая эмульсия</option>
<option value="34">Морковная маска</option>
<option value="30">Excite Power Plus</option>
<option value="35">Провокация крем</option>
<option value="37">Hair removal</option>
<option value="38">Активатор рыбного клева</option>
<option value="8">Green Coffee</option>
<option value="39">XXXL Cream</option>
<option value="40">ПБК-20</option>
<option value="41">VaryForte</option>
<option value="42">OsteoRen</option>
<option value="43">Jarabe Dren</option>
<option value="44">Liquid Stop</option>
<option value="45">Saciante</option>
<option value="1">Goji Berries</option>
<option value="14">Toe PRO</option>
<option value="3">Breast Cream</option>
<option value="20">Antiaging cream</option>
<option value="47">Titan Gel</option>
<option value="46">MaxLift Day</option>
<option value="48">MaxLift Night</option>
<option value="49">VaryForte +</option>
<option value="50">Forskolin 2</option>
<option value="51">Braslet Xpower </option>
<option value="52">Heart of ocean</option>
<option value="53">Toretto Necklace </option>
<option value="54">Thor's Hammer</option>
<option value="55">Fungalor</option>
<option value="56">Fixline Detox </option>
<option value="57">Fixline  Weight Control </option>
<option value="59">BigBust Firming Up</option>
<option value="58">BigBust Shape Up</option>
</select>
                                
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="Group by" class="col-sm-2 control-label">Group by:</label>
                            <div class="col-sm-5">
                                <select class="form-control select2 select2-hidden-accessible" id="id_group_by" name="group_by" tabindex="-1" aria-hidden="true">
<option value="day" selected="selected">Day</option>
<option value="hour">Hour</option>
<option value="goods">Goods</option>
<option value="country">Country</option>
<option value="operator">Operator</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 503px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_group_by-container"><span class="select2-selection__rendered" id="select2-id_group_by-container" title="Day">Day</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                
                            </div>
                        </div>

                        <script>
                            $(document).ready(function(){
                                $('.datefield input').datepicker({
                                    format: 'yyyy-mm-dd'
                                });
                            });
                        </script>
                        <div class="form-group  ">
                            <label class="col-sm-2 control-label">Date range:</label>
                            <div class="col-sm-5">
                                <div class="input-group datefield" style="width: 52%; float: left;">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input class="form-control" id="id_date_from" name="date_from" type="text" value="2016-02-15">
                                </div>

                                <div class="input-group datefield" style="width: 48%;">
                                    <input class="form-control" id="id_date_to" name="date_to" type="text" value="2016-02-22">
                                </div>

                                

                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-2">
                                <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                    <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                </button>
                            </div>
                        </div>

                    </div>
                </form>
                <!-- /.box-header -->
                <div class="box-header">
                  <h3 class="box-title">Statistic</h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover" style="font-size: 11px;">
                        <tbody><tr>
                            <th rowspan="2">Группировка</th>
                            <th colspan="7">Заявки</th>
                            <th colspan="7">Заказы</th>
                            <th colspan="10">Финансы</th>
                        </tr>
                        <tr>
                            <th>Всего</th>
                            <th>Подтвержден (%)</th>
                            <th>UpSale (%)</th>
                            <th>Обработка (%)</th>
                            <th>Отказ (%)</th>
                            <th>Дубликат (+авто)</th>
                            <th>Треш (%)</th>

                            <th>Всего</th>
                            <th>Всего товаров</th>
                            <th>Ожидает отправки</th>
                            <th>Отправлено</th>
                            <th>Доставляется</th>
                            <th>Выкуплено (%)</th>
                            <th>Возврат (%)</th>

                            <th>Общая сумма</th>
                            <th>Выкуплено (%)</th>
                            <th>Прогноз (%)</th>
                            <th>Доставка</th>
                            <th>Возвраты</th>
                            <th>Себестоимость</th>
                            <th>VAT</th>
                            <th>PPL</th>
                            <th>CPL</th>
                            <th>PFT</th>
                        </tr>
                        
                            <tr>
                                
                                    <td>2016-02-15</td>
                                
                                <td>544</td>
                                <td>319(59%)</td>
                                <td>181(57%)</td>
                                <td>26(5%)</td>
                                <td>123(23%)</td>
                                <td>19(3%)</td>
                                <td>57(10%)</td>

                                <td>319</td>
                                <td>181</td>
                                <td>32</td>
                                <td>0</td>
                                <td>65</td>
                                <td>208(65%)</td>
                                <td>14(4%)</td>

                                <td>14502,25</td>
                                <td>9663,25</td>
                                
                                    <td>11601,8(80%)</td>
                                
                                <td>1595,0</td>
                                <td>70,0</td>
                                <td>1688,51</td>
                                <td>9663,25</td>
                                <td>4250,0</td>
                                <td>0,0</td>
                                <td>-6008,51</td>
                            </tr>
                        
                            <tr>
                                
                                    <td>2016-02-16</td>
                                
                                <td>541</td>
                                <td>309(57%)</td>
                                <td>207(67%)</td>
                                <td>35(6%)</td>
                                <td>114(21%)</td>
                                <td>18(3%)</td>
                                <td>65(12%)</td>

                                <td>309</td>
                                <td>207</td>
                                <td>128</td>
                                <td>0</td>
                                <td>30</td>
                                <td>144(47%)</td>
                                <td>7(2%)</td>

                                <td>14565,0</td>
                                <td>6866,75</td>
                                
                                    <td>11652,0(80%)</td>
                                
                                <td>1545,0</td>
                                <td>35,0</td>
                                <td>1749,68</td>
                                <td>6866,75</td>
                                <td>2679,0</td>
                                <td>0,0</td>
                                <td>-4463,68</td>
                            </tr>
                        
                            <tr>
                                
                                    <td>2016-02-17</td>
                                
                                <td>587</td>
                                <td>306(52%)</td>
                                <td>188(61%)</td>
                                <td>49(8%)</td>
                                <td>137(23%)</td>
                                <td>26(4%)</td>
                                <td>69(12%)</td>

                                <td>306</td>
                                <td>188</td>
                                <td>132</td>
                                <td>0</td>
                                <td>46</td>
                                <td>120(39%)</td>
                                <td>8(3%)</td>

                                <td>14166,25</td>
                                <td>5532,5</td>
                                
                                    <td>11333,0(80%)</td>
                                
                                <td>1530,0</td>
                                <td>40,0</td>
                                <td>1688,05</td>
                                <td>5532,5</td>
                                <td>2895,0</td>
                                <td>0,0</td>
                                <td>-4623,05</td>
                            </tr>
                        
                            <tr>
                                
                                    <td>2016-02-18</td>
                                
                                <td>700</td>
                                <td>341(49%)</td>
                                <td>219(64%)</td>
                                <td>71(10%)</td>
                                <td>139(20%)</td>
                                <td>26(4%)</td>
                                <td>123(18%)</td>

                                <td>341</td>
                                <td>219</td>
                                <td>126</td>
                                <td>0</td>
                                <td>134</td>
                                <td>77(23%)</td>
                                <td>4(1%)</td>

                                <td>16093,0</td>
                                <td>3644,0</td>
                                
                                    <td>12874,4(80%)</td>
                                
                                <td>1700,0</td>
                                <td>20,0</td>
                                <td>1877,58</td>
                                <td>3644,0</td>
                                <td>2685,0</td>
                                <td>0,0</td>
                                <td>-4582,58</td>
                            </tr>
                        
                            <tr>
                                
                                    <td>2016-02-19</td>
                                
                                <td>594</td>
                                <td>304(51%)</td>
                                <td>208(68%)</td>
                                <td>107(18%)</td>
                                <td>91(15%)</td>
                                <td>20(3%)</td>
                                <td>72(12%)</td>

                                <td>304</td>
                                <td>208</td>
                                <td>125</td>
                                <td>3</td>
                                <td>147</td>
                                <td>29(10%)</td>
                                <td>0(0%)</td>

                                <td>14525,25</td>
                                <td>1405,25</td>
                                
                                    <td>11474,95(79%)</td>
                                
                                <td>1520,0</td>
                                <td>0</td>
                                <td>1728,39</td>
                                <td>1405,25</td>
                                <td>2610,0</td>
                                <td>0,0</td>
                                <td>-4338,39</td>
                            </tr>
                        
                            <tr>
                                
                                    <td>2016-02-20</td>
                                
                                <td>639</td>
                                <td>256(40%)</td>
                                <td>164(64%)</td>
                                <td>206(32%)</td>
                                <td>77(12%)</td>
                                <td>11(2%)</td>
                                <td>89(14%)</td>

                                <td>256</td>
                                <td>164</td>
                                <td>192</td>
                                <td>0</td>
                                <td>64</td>
                                <td>0(0%)</td>
                                <td>0(0%)</td>

                                <td>12000,75</td>
                                <td>0</td>
                                
                                    <td>9480,59(79%)</td>
                                
                                <td>1280,0</td>
                                <td>0</td>
                                <td>1441,56</td>
                                <td>0</td>
                                <td>2415,0</td>
                                <td>0,0</td>
                                <td>-3856,56</td>
                            </tr>
                        
                            <tr>
                                
                                    <td>2016-02-21</td>
                                
                                <td>514</td>
                                <td>182(35%)</td>
                                <td>88(48%)</td>
                                <td>222(43%)</td>
                                <td>48(9%)</td>
                                <td>9(2%)</td>
                                <td>53(10%)</td>

                                <td>182</td>
                                <td>88</td>
                                <td>182</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0(0%)</td>
                                <td>0(0%)</td>

                                <td>8132,25</td>
                                <td>0</td>
                                
                                    <td>6343,16(78%)</td>
                                
                                <td>910,0</td>
                                <td>0</td>
                                <td>947,47</td>
                                <td>0</td>
                                <td>2040,0</td>
                                <td>0,0</td>
                                <td>-2987,47</td>
                            </tr>
                        
                            <tr>
                                
                                    <td>2016-02-22</td>
                                
                                <td>0</td>
                                <td>0(0%)</td>
                                <td>0(0%)</td>
                                <td>0(0%)</td>
                                <td>0(0%)</td>
                                <td>0(0%)</td>
                                <td>0(0%)</td>

                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0(0%)</td>
                                <td>0(0%)</td>

                                <td>0</td>
                                <td>0</td>
                                
                                    <td>0,0(78%)</td>
                                
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                                <td>0</td>
                            </tr>
                        
                            <tr>
                                
                                    <td><strong>Summary</strong></td>
                                
                                <td>4119</td>
                                <td>2017(49%)</td>
                                <td>1255(62%)</td>
                                <td>716(17%)</td>
                                <td>729(18%)</td>
                                <td>129(3%)</td>
                                <td>528(13%)</td>

                                <td>2017</td>
                                <td>1255</td>
                                <td>917</td>
                                <td>3</td>
                                <td>486</td>
                                <td>578(29%)</td>
                                <td>33(2%)</td>

                                <td>93984,75</td>
                                <td>27111,75</td>
                                
                                    <td>74759,9</td>
                                
                                <td>10080,0</td>
                                <td>165,0</td>
                                <td>11121,24</td>
                                <td>27111,75</td>
                                <td>19574,0</td>
                                <td>0,0</td>
                                <td>-30860,24</td>
                            </tr>
                        
                    </tbody></table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
