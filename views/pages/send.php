<div class="row">
<div class="col-md-offset-1 col-md-10 col-md-offset-1">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Compose New Message</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="form-group">
                    <input class="form-control" type="email" id="to" placeholder="To:">
                  </div>

                  <div class="form-group">
                    <input class="form-control" id="subject" placeholder="Subject:">
                  </div>
                  <div class="form-group">

                    <textarea id="compose-textarea" id="text" class="form-control" style="height: 300px">
                      
                    </textarea>
                  </div>
                  
                </div><!-- /.box-body -->

                <div class="box-footer">
                  
                  
                </div><!-- /.box-footer -->
              </div><!-- /. box -->
              <div class="pull-right">
                    
                    <button type="submit" id="send" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
                  </div>
            </div><!-- /.col -->
	
</div>
<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <script>
      $("#compose-textarea").wysihtml5();

        $('#send').on('click', function(e)
        {
            e.preventDefault();
            var mail = $('#to').val();
            var subject = $('#subject').val();
            var text = $('textarea').val();
            console.log(mail, subject, text);

            $.ajax({
      				type:'POST',
      				url:'views/ajax/send_mail.php',
      				data: 'mail='+mail+'&subject='+subject+'&text='+text,
    					
              success:function(result) { 
              	console.log(result);
                
               
              } 

            });

          return false;
        });
    </script>