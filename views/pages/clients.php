<section class="content-header">
    <h1>
        Clients
        <small>clients table</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="http://admin-crm.com/panel/clients/#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Clients</li>
    </ol>
</section>
<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">





                </div>
                <div class="box-body table-responsive">
					<table class="table table-hover table-vcenter" id="function">
						<thead>
							<tr role="row">
                                <th style="width:160px;"><input class="form-control" id="id_id" name="id" type="text"></th>
								<th style="width:160px;"><input class="form-control" id="id_phone" maxlength="512" name="phone" type="text"></th>
								<th style="width:300px;"><input class="form-control" id="id_name" maxlength="1024" name="name" type="text"></th>
                                <th style="width:160px;"><select class="form-control select2 select2-hidden-accessible" id="id_country" name="country" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="3">Румыния</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 148px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_country-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
                                <th style="width:160px;"><input class="form-control" id="id_city" name="city" type="text"></th>
                                <th style="width:160px;"><input class="form-control" id="id_zipcode" maxlength="16" name="zipcode" type="text"></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
                                <th>ID</th>
								<th>Telefon</th>
								<th>Name</th>
                                <th>Tară</th>
                                <th>city</th>
                                <th>Zipcode</th>
                                <th style="width: 116px; min-width:116px;"></th>
							</tr>
						</thead>
						<tbody>
						
							
              		<? foreach ($users as $key => $value): ?>
                    <tr>
                        <td><?=$value['id']?></td>
                        <td><?=$value['phone']?></td>
                        <td><?=$value['name']?></td> 
                        <td><?=$value['country']?></td>
                        <td><?=$value['city']?></td>
                        <td><?=$value['zip_code']?></td>
                        <td>
	                        <div class="btn-group">
	                            <a target="_blank" href="http://admin-crm.com/panel/edit_order/create/?country_id=3&amp;client_id=532319" data-id="<?=$value['id']?>" class="glyphicon glyphicon-new-window btn btn-default btn-sm">Add order</a>
	                        </div>
	                    </td>
                    </tr>

                  <? endforeach; ?>

						</tbody>
					</table>

                   

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
<!-- /.content -->

    </div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
// <script>
//       $(function () {
       
//         $('#function').DataTable({
//           "paging": true,
//           "lengthChange": false,
//           "searching": true,
//           "ordering": true,
//           "info": true,
//           "autoWidth": false
//         });
//       });
//     </script>