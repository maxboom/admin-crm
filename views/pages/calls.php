
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Calls
        <small>calls table</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Calls</li>
    </ol>
</section>
<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">





                </div>
                <div class="box-body table-responsive">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
                                <th><input class="form-control" id="id_id" name="id" type="text"></th>
                                <th><select class="form-control select2 select2-hidden-accessible" id="id_country" name="country" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="3">Румыния</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 121px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_country-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
								<th><div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input id="id_proccessed" name="proccessed" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></th>
								<th><select class="form-control select2 select2-hidden-accessible" id="id_type" name="type" tabindex="-1" aria-hidden="true">
<option value="1">Outcoming</option>
<option value="2">Incoming</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 126px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_type-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>

                                <th><input class="form-control" id="id_order" name="order" type="text"></th>
                                <th></th>
                                <th><input class="form-control" id="id_phone" maxlength="512" name="phone" type="text"></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
                                <th>ID</th>
                                <th>Tară</th>
								<th>Proccessed</th>
								<th>Type</th>

                                <th>Ordine</th>
                                <th>Path</th>
                                <th>Telefon</th>
                                <th>Created</th>
                                <th style="width: 116px; min-width:116px;"></th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>

                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
<!-- /.content -->
