
<section class="content-header">
    <h1>
        Orders
        <small>Orders to send ro</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Orders</li>
    </ol>
</section>

<form id="packing">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">
                    <div class="btn-group">
                        <button data-url="/panel/packing/return_to_pack/" id="return_to_pack" type="button" class="btn btn-warning"><span class="glyphicon glyphicon-remove"></span> Return to Packing</button>
                    </div>
                    <div class="btn-group">
                        <button data-url="/panel/packing/ro/print_invoice/?" id="print" type="button" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print Invoce</button>
                    </div>
                    <div class="btn-group">
                        <button data-url="/panel/packing/ro/print_fan/?" id="print_fan" type="button" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print FAN</button>
                    </div>
                </div>
                <div class="box-body table-responsive">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
                                <th>
                                    <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" id="check_all" name="check_all" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div>
                                </th>
								<th style="width:100px;"><input value="" name="id" class="form-control" type="text"></th>
								<th></th>
								<th><input class="form-control" id="id_trackid" maxlength="128" name="trackid" type="text"></th>
								<th><input class="form-control" id="id_name" maxlength="1024" name="name" type="text"></th>
								<th></th>

								<th style="width:80px;"></th>

								<th></th>
								<th style="min-width:100px; max-width:100px;">
									<select class="form-control select2 select2-hidden-accessible" id="id_delivery" name="delivery" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="4">Почта Румынии</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 84px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_delivery-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
								</th>
								<th style="min-width:100px; max-width:100px;"></th>
								<th style="min-width:100px; max-width:100px;"></th>
								<th style="min-width:100px; max-width:100px;"></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
								<th>Ch</th>
								<th>ID</th>
								<th>Goods</th>
								<th>TrackId</th>
								<th>Name</th>

								<th>Dep.</th>
								<th>Preț</th>

								<th>Oraș</th>
								<th>Livrare</th>
								<th>Created</th>
								<th>Modified</th>
								<th>Send</th>
                                <th></th>
							</tr>
						</thead>
						<tbody>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="566598" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>566598</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-566598" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-566598">6051649360005</span>
								</td>
								<td>Zimbran Doru</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Resita</td>
								<td>Почта Румынии</td>
								<td>
									2016-02-19<br>
									20:26
								</td>
								<td>
									2016-02-20<br>
									10:09
								</td>
								<td>
									2016-02-19<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/566598" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="566596" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>566596</td>
								<td>Breast Cream</td>
								<td>
									<div id="spinner-566596" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-566596">6051649360031</span>
								</td>
								<td>Turnea gheorghe</td>

								<td> </td>
								<td>175,0 / 1</td>

								<td>23 August</td>
								<td>Почта Румынии</td>
								<td>
									2016-02-19<br>
									20:26
								</td>
								<td>
									2016-02-20<br>
									10:28
								</td>
								<td>
									2016-02-19<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/566596" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="566222" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>566222</td>
								<td>VaryForte</td>
								<td>
									<div id="spinner-566222" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-566222">6051649360002</span>
								</td>
								<td>Sorin Oarcea (SC ART STIL METAL)</td>

								<td> </td>
								<td>245,0 / 2</td>

								<td>Arad</td>
								<td>Почта Румынии</td>
								<td>
									2016-02-19<br>
									18:29
								</td>
								<td>
									2016-02-20<br>
									10:01
								</td>
								<td>
									2016-02-19<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/566222" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="539759" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>539759</td>
								<td>Antiaging cream</td>
								<td>
									<div id="spinner-539759" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-539759">6042649360126</span>
								</td>
								<td>KOLUMBAN EVA</td>

								<td> </td>
								<td>155,0 / 1</td>

								<td>Talisoara</td>
								<td>Почта Румынии</td>
								<td>
									2016-02-11<br>
									10:32
								</td>
								<td>
									2016-02-11<br>
									12:06
								</td>
								<td>
									2016-02-11<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/539759" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="537978" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>537978</td>
								<td>Goji Berries</td>
								<td>
									<div id="spinner-537978" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-537978">6041649360495</span>
								</td>
								<td>claudia craciunean</td>

								<td> </td>
								<td>119,0 / 1</td>

								<td>Alba Iulia</td>
								<td>Почта Румынии</td>
								<td>
									2016-02-10<br>
									17:34
								</td>
								<td>
									2016-02-10<br>
									17:51
								</td>
								<td>
									2016-02-13<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/537978" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="537927" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>537927</td>
								<td>Breast Cream</td>
								<td>
									<div id="spinner-537927" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-537927">6041649360491</span>
								</td>
								<td>Radu Verdeanu sc Vita Puls srl, RO 28658988</td>

								<td> </td>
								<td>175,0 / 1</td>

								<td>Piatra-Neamt</td>
								<td>Почта Румынии</td>
								<td>
									2016-02-10<br>
									17:16
								</td>
								<td>
									2016-02-10<br>
									17:35
								</td>
								<td>
									2016-02-10<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/537927" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="537832" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>537832</td>
								<td>Goji Berries</td>
								<td>
									<div id="spinner-537832" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-537832">6041649360494</span>
								</td>
								<td>Haidau Diana</td>

								<td> </td>
								<td>219,0 / 3</td>

								<td>Suceava</td>
								<td>Почта Румынии</td>
								<td>
									2016-02-10<br>
									16:42
								</td>
								<td>
									2016-02-10<br>
									17:51
								</td>
								<td>
									2016-02-10<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/537832" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="511151" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>511151</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-511151" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-511151">6032649360394</span>
								</td>
								<td>evsei viorel</td>

								<td> <i class="fa-fw fa fa-check"></i></td>
								<td>219,0 / 2</td>

								<td>Tulcea</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-31<br>
									16:05
								</td>
								<td>
									2016-02-01<br>
									13:40
								</td>
								<td>
									2016-01-31<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/511151" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="509872" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>509872</td>
								<td>Titan Gel</td>
								<td>
									<div id="spinner-509872" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-509872">6032649360288</span>
								</td>
								<td>Crisan Sorin</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Dej</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-31<br>
									1:16
								</td>
								<td>
									2016-02-01<br>
									12:58
								</td>
								<td>
									2016-01-31<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/509872" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="505629" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>505629</td>
								<td>Electricity Saving Box</td>
								<td>
									<div id="spinner-505629" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-505629">6029649360223</span>
								</td>
								<td>Cristi</td>

								<td> </td>
								<td>200,0 / 1</td>

								<td>Ramnicu Valcea</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-29<br>
									5:19
								</td>
								<td>
									2016-01-29<br>
									14:27
								</td>
								<td>
									2016-02-01<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/505629" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="486799" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>486799</td>
								<td>Teeth Whitening Pen</td>
								<td>
									<div id="spinner-486799" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-486799"></span>
								</td>
								<td>Matragociu Daniel*</td>

								<td> </td>
								<td>249,0 / 2</td>

								<td>Plopeni</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-20<br>
									8:07
								</td>
								<td>
									2016-01-21<br>
									14:35
								</td>
								<td>
									2016-01-21<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/486799" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="476567" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>476567</td>
								<td>Antiaging cream</td>
								<td>
									<div id="spinner-476567" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-476567">6015649360307</span>
								</td>
								<td>Georgiana Mitrana </td>

								<td> </td>
								<td>290,0 / 2</td>

								<td>Mizil</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-15<br>
									13:22
								</td>
								<td>
									2016-01-15<br>
									17:46
								</td>
								<td>
									2016-01-15<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/476567" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="463970" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>463970</td>
								<td>Breast Cream</td>
								<td>
									<div id="spinner-463970" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-463970">6009649360121</span>
								</td>
								<td>Barbu Angelica Simona</td>

								<td> </td>
								<td>175,0 / 1</td>

								<td>Maxineni</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-09<br>
									12:47
								</td>
								<td>
									2016-01-09<br>
									13:26
								</td>
								<td>
									2016-01-09<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/463970" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="463958" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>463958</td>
								<td>Breast Cream</td>
								<td>
									<div id="spinner-463958" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-463958">6009649360117</span>
								</td>
								<td>Godja iuliana</td>

								<td> </td>
								<td>249,0 / 2</td>

								<td>Sighetu Marmatiei</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-09<br>
									12:40
								</td>
								<td>
									2016-01-09<br>
									13:18
								</td>
								<td>
									2016-01-12<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/463958" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="462181" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>462181</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-462181" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-462181">6009649360120</span>
								</td>
								<td>Barbu  Angelica Simona </td>

								<td> </td>
								<td>160,0 / 1</td>

								<td>Maxineni</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-08<br>
									13:52
								</td>
								<td>
									2016-01-09<br>
									13:26
								</td>
								<td>
									2016-01-09<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/462181" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="458381" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>458381</td>
								<td>Antiaging cream</td>
								<td>
									<div id="spinner-458381" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-458381">6006649360352</span>
								</td>
								<td>Dinu Valy Luciana</td>

								<td> </td>
								<td>225,0 / 2</td>

								<td>Ploiesti</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-06<br>
									13:42
								</td>
								<td>
									2016-01-06<br>
									14:27
								</td>
								<td>
									2016-01-06<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/458381" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="458364" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>458364</td>
								<td>Breast Cream</td>
								<td>
									<div id="spinner-458364" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-458364">6006649360351</span>
								</td>
								<td>Dinu Valy Luciana</td>

								<td> </td>
								<td>319,0 / 3</td>

								<td>Ploiesti</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-06<br>
									13:31
								</td>
								<td>
									2016-01-06<br>
									14:27
								</td>
								<td>
									2016-01-06<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/458364" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="455528" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>455528</td>
								<td>Breast Cream</td>
								<td>
									<div id="spinner-455528" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-455528">6005649360272</span>
								</td>
								<td>Ivar Florina</td>

								<td> </td>
								<td>175,0 / 1</td>

								<td>Targoviste</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-04<br>
									22:32
								</td>
								<td>
									2016-01-05<br>
									15:44
								</td>
								<td>
									2016-01-05<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/455528" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="454897" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>454897</td>
								<td>Toe Fix</td>
								<td>
									<div id="spinner-454897" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-454897">6005649360076</span>
								</td>
								<td>Raducanu cecilia Camelia</td>

								<td> </td>
								<td>249,0 / 2</td>

								<td>Petrosani</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-04<br>
									16:28
								</td>
								<td>
									2016-01-05<br>
									13:40
								</td>
								<td>
									2016-01-04<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/454897" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="454728" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>454728</td>
								<td>Breast Cream</td>
								<td>
									<div id="spinner-454728" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-454728">6004649360108</span>
								</td>
								<td>Duta ioana alexandra</td>

								<td> </td>
								<td>175,0 / 1</td>

								<td>Magurele</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-04<br>
									14:48
								</td>
								<td>
									2016-01-04<br>
									15:09
								</td>
								<td>
									2016-01-04<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/454728" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="454316" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>454316</td>
								<td>Antiaging cream</td>
								<td>
									<div id="spinner-454316" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-454316">6011649360250</span>
								</td>
								<td>SARIVAN MARIA</td>

								<td> </td>
								<td>295,0 / 3</td>

								<td>Constanta</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-04<br>
									10:09
								</td>
								<td>
									2016-01-11<br>
									13:11
								</td>
								<td>
									2016-01-11<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/454316" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="450678" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>450678</td>
								<td>Teeth Whitening Pen</td>
								<td>
									<div id="spinner-450678" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-450678">6005649360075</span>
								</td>
								<td>Raducanu cecilia Camelia</td>

								<td> </td>
								<td>249,0 / 2</td>

								<td>Petrosani</td>
								<td>Почта Румынии</td>
								<td>
									2016-01-02<br>
									15:16
								</td>
								<td>
									2016-01-05<br>
									13:40
								</td>
								<td>
									2016-01-04<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/450678" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="447468" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>447468</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-447468" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-447468">6004649360099</span>
								</td>
								<td>Ion Nicolae</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Tronari</td>
								<td>Почта Румынии</td>
								<td>
									2015-12-30<br>
									20:41
								</td>
								<td>
									2016-01-06<br>
									12:22
								</td>
								<td>
									2016-01-04<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/447468" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="447461" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>447461</td>
								<td>Breast Cream</td>
								<td>
									<div id="spinner-447461" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-447461">6004649360112</span>
								</td>
								<td>Madalina garoafa</td>

								<td> </td>
								<td>175,0 / 1</td>

								<td>Targu Jiu</td>
								<td>Почта Румынии</td>
								<td>
									2015-12-30<br>
									20:35
								</td>
								<td>
									2016-01-05<br>
									12:33
								</td>
								<td>
									2016-01-05<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/447461" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
							<tr>
                                <td>
	                                <div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input class="orders-check-box" name="order_id[]" value="447457" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></td>
								<td>447457</td>
								<td>XXL Power Life</td>
								<td>
									<div id="spinner-447457" class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>
									<span id="trackid-447457">6004649360100</span>
								</td>
								<td>Vasile</td>

								<td> </td>
								<td>219,0 / 2</td>

								<td>Milisauti</td>
								<td>Почта Румынии</td>
								<td>
									2015-12-30<br>
									20:34
								</td>
								<td>
									2016-01-06<br>
									12:22
								</td>
								<td>
									2016-01-04<br>
								</td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/orders/447457" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>


                                    </div>
                                </td>
							</tr>
						
						</tbody>
					</table>

                    


<ul class="pagination">

  <li class="disabled"><a href="#" onclick="javascript: return false;">«</a></li>


  
    
  <li class="active"><a href="#" onclick="javascript: return false;">1</a></li>
    
  

  
    
  <li><a href="?page=2">2</a></li>
    
  

  
    
  <li><a href="?page=3">3</a></li>
    
  

  
    
  <li><a href="?page=4">4</a></li>
    
  

  
  <li class="disabled"><a href="#" onclick="javascript: return false;">...</a></li>
  

  
    
  <li><a href="?page=11">11</a></li>
    
  

  
    
  <li><a href="?page=12">12</a></li>
    
  

  
    
  <li><a href="?page=13">13</a></li>
    
  

  
    
  <li><a href="?page=14">14</a></li>
    
  


  <li><a href="?page=2">»</a></li>

</ul>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
