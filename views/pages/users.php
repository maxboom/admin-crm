
<section class="content-header">
    <h1>
        Users
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Acasă</a></li>
        <li class="active">Accounts</li>
        <li class="active">Users</li>
    </ol>
</section>

<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">
                    <a href="/panel/users/create/" class="btn btn-sm btn-primary">
                        <span class="glyphicon glyphicon-new-window"></span> <b>New </b>
                    </a>
                </div>
                <!-- /.box-body -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover table-vcenter ">
                        <tbody><tr>





                            <th style="width: 120px;">
                                <input name="id" value="" class="form-control" type="text">
                            </th>
                            <th><input class="form-control" id="id_username" maxlength="30" name="username" type="text"></th>
                            <th><input class="form-control" id="id_email" name="email" type="text"></th>
                            <th><input class="form-control" id="id_first_name" maxlength="30" name="first_name" type="text"></th>
                            <th><input class="form-control" id="id_last_name" maxlength="30" name="last_name" type="text"></th>
                            <th><div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input id="id_is_active" name="is_active" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></th>
                            <th style="width: 125px; min-width:125px;">
                                <button type="submit" class="btn btn-block btn-success">
                                    <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                </button>
                            </th>
                        </tr>
                        <tr>

                            <th>Id</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Is Active</th>
                            <th>Action</th>
                        </tr>
                        
                        <tr>





                            <td>49</td>
                            <td>or10</td>
                            <td></td>
                            <td>Pena</td>
                            <td>Adelina</td>
                            <th><i class="fa-fw fa fa-check"></i></th>
                            <td>
                                <div class="btn-group">
                                    <a href="/panel/users/edit/49/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    <a href="/panel/users/show/49/" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>
                                    <a href="/panel/users/delete/49/" class="glyphicon glyphicon-trash btn btn-default btn-sm"></a>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>





                            <td>54</td>
                            <td>opl3</td>
                            <td></td>
                            <td>Khelifa</td>
                            <td>Agnieszka</td>
                            <th><i class="fa-fw fa fa-check"></i></th>
                            <td>
                                <div class="btn-group">
                                    <a href="/panel/users/edit/54/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    <a href="/panel/users/show/54/" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>
                                    <a href="/panel/users/delete/54/" class="glyphicon glyphicon-trash btn btn-default btn-sm"></a>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>





                            <td>50</td>
                            <td>user_ro</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th><i class="fa-fw fa fa-check"></i></th>
                            <td>
                                <div class="btn-group">
                                    <a href="/panel/users/edit/50/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    <a href="/panel/users/show/50/" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>
                                    <a href="/panel/users/delete/50/" class="glyphicon glyphicon-trash btn btn-default btn-sm"></a>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>





                            <td>51</td>
                            <td>buhalter</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th><i class="fa-fw fa fa-check"></i></th>
                            <td>
                                <div class="btn-group">
                                    <a href="/panel/users/edit/51/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    <a href="/panel/users/show/51/" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>
                                    <a href="/panel/users/delete/51/" class="glyphicon glyphicon-trash btn btn-default btn-sm"></a>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>





                            <td>40</td>
                            <td>or3</td>
                            <td></td>
                            <td>Tatiana</td>
                            <td>Ciorba</td>
                            <th><i class="fa-fw fa fa-check"></i></th>
                            <td>
                                <div class="btn-group">
                                    <a href="/panel/users/edit/40/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    <a href="/panel/users/show/40/" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>
                                    <a href="/panel/users/delete/40/" class="glyphicon glyphicon-trash btn btn-default btn-sm"></a>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>





                            <td>56</td>
                            <td>romania</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th><i class="fa-fw fa fa-check"></i></th>
                            <td>
                                <div class="btn-group">
                                    <a href="/panel/users/edit/56/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    <a href="/panel/users/show/56/" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>
                                    <a href="/panel/users/delete/56/" class="glyphicon glyphicon-trash btn btn-default btn-sm"></a>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>





                            <td>38</td>
                            <td>or2</td>
                            <td></td>
                            <td>Burdui </td>
                            <td>Tatiana</td>
                            <th><i class="fa-fw fa fa-check"></i></th>
                            <td>
                                <div class="btn-group">
                                    <a href="/panel/users/edit/38/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    <a href="/panel/users/show/38/" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>
                                    <a href="/panel/users/delete/38/" class="glyphicon glyphicon-trash btn btn-default btn-sm"></a>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>





                            <td>32</td>
                            <td>serious</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th><i class="fa-fw fa fa-check"></i></th>
                            <td>
                                <div class="btn-group">
                                    <a href="/panel/users/edit/32/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    <a href="/panel/users/show/32/" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>
                                    <a href="/panel/users/delete/32/" class="glyphicon glyphicon-trash btn btn-default btn-sm"></a>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>





                            <td>18</td>
                            <td>anton</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <th></th>
                            <td>
                                <div class="btn-group">
                                    <a href="/panel/users/edit/18/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    <a href="/panel/users/show/18/" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>
                                    <a href="/panel/users/delete/18/" class="glyphicon glyphicon-trash btn btn-default btn-sm"></a>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>





                            <td>25</td>
                            <td>op6</td>
                            <td></td>
                            <td>Турчина</td>
                            <td>Оксана</td>
                            <th><i class="fa-fw fa fa-check"></i></th>
                            <td>
                                <div class="btn-group">
                                    <a href="/panel/users/edit/25/" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                    <a href="/panel/users/show/25/" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>
                                    <a href="/panel/users/delete/25/" class="glyphicon glyphicon-trash btn btn-default btn-sm"></a>
                                </div>
                            </td>
                        </tr>
                        
                    </tbody></table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    


<ul class="pagination">

  <li class="disabled"><a href="#" onclick="javascript: return false;">«</a></li>


  
    
  <li class="active"><a href="#" onclick="javascript: return false;">1</a></li>
    
  

  
    
  <li><a href="?page=2">2</a></li>
    
  

  
    
  <li><a href="?page=3">3</a></li>
    
  

  
    
  <li><a href="?page=4">4</a></li>
    
  

  
  <li class="disabled"><a href="#" onclick="javascript: return false;">...</a></li>
  

  
    
  <li><a href="?page=15">15</a></li>
    
  

  
    
  <li><a href="?page=16">16</a></li>
    
  

  
    
  <li><a href="?page=17">17</a></li>
    
  

  
    
  <li><a href="?page=18">18</a></li>
    
  


  <li><a href="?page=2">»</a></li>

</ul>

                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
