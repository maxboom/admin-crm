
<section class="content-header">
    <h1>
	    Operator bonus
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Operator bonus</li>
    </ol>
</section>

<form class="">
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Month: </label>
                                <input class="form-control monthyearpicker" data-date-format="yyyy-mm" id="id_month" name="month" type="text">
                            </div>
                        </div>
                        <div class="col-sm-1 pull-right text-right">
                            <div class="form-group">
                                <label></label>
                                <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                    <span class="glyphicon glyphicon-filter"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
                                <th>User</th>
                                <th>bonus</th>
							</tr>
						</thead>
						<tbody>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/39">or1</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/49">or10</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/58">or11</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/59">or12</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/156">or13</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/157">or14</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/38">or2</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/40">or3</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/42">or4</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/43">or5</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/45">or6</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/46">or7</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/47">or8</a></td>
								<td>0</td>
							</tr>
						
							<tr>
								<td><a href="/panel/operator_bonus/edit/48">or9</a></td>
								<td>0</td>
							</tr>
						
						</tbody>
					</table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
