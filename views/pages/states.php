
<section class="content-header">
    <h1>
        States
        <small>States list</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">States</li>
    </ol>
</section>

<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">
                    <a href="/panel/states/create/" class="btn btn-sm btn-primary">
                        <span class="glyphicon glyphicon-new-window"></span> <b>New </b>
                    </a>
                </div>
                <div class="box-body table-responsive">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">

								<th><input class="form-control" id="id_name" maxlength="255" name="name" type="text"></th>
								<th style="width:160px;"><select class="form-control select2 select2-hidden-accessible" id="id_country" name="country" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="3">Румыния</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 144px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_country-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">

								<th>Nume</th>
								<th>Tară</th>
                                <th></th>
							</tr>
						</thead>
						<tbody>
						
							<tr>

								<td>Ilfov</td>
								<td><a href="/panel/countries/edit/3/">Румыния</a></td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/states/edit/98" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        <a href="/panel/cities/?filter=filter&amp;state=98" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>

                                    </div>
                                </td>
							</tr>
						
							<tr>

								<td>Vrancea</td>
								<td><a href="/panel/countries/edit/3/">Румыния</a></td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/states/edit/97" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        <a href="/panel/cities/?filter=filter&amp;state=97" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>

                                    </div>
                                </td>
							</tr>
						
							<tr>

								<td>Vaslui</td>
								<td><a href="/panel/countries/edit/3/">Румыния</a></td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/states/edit/96" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        <a href="/panel/cities/?filter=filter&amp;state=96" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>

                                    </div>
                                </td>
							</tr>
						
							<tr>

								<td>Valcea</td>
								<td><a href="/panel/countries/edit/3/">Румыния</a></td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/states/edit/95" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        <a href="/panel/cities/?filter=filter&amp;state=95" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>

                                    </div>
                                </td>
							</tr>
						
							<tr>

								<td>Tulcea</td>
								<td><a href="/panel/countries/edit/3/">Румыния</a></td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/states/edit/94" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        <a href="/panel/cities/?filter=filter&amp;state=94" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>

                                    </div>
                                </td>
							</tr>
						
							<tr>

								<td>Timis</td>
								<td><a href="/panel/countries/edit/3/">Румыния</a></td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/states/edit/93" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        <a href="/panel/cities/?filter=filter&amp;state=93" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>

                                    </div>
                                </td>
							</tr>
						
							<tr>

								<td>Teleorman</td>
								<td><a href="/panel/countries/edit/3/">Румыния</a></td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/states/edit/92" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        <a href="/panel/cities/?filter=filter&amp;state=92" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>

                                    </div>
                                </td>
							</tr>
						
							<tr>

								<td>Suceava</td>
								<td><a href="/panel/countries/edit/3/">Румыния</a></td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/states/edit/91" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        <a href="/panel/cities/?filter=filter&amp;state=91" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>

                                    </div>
                                </td>
							</tr>
						
							<tr>

								<td>Sibiu</td>
								<td><a href="/panel/countries/edit/3/">Румыния</a></td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/states/edit/90" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        <a href="/panel/cities/?filter=filter&amp;state=90" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>

                                    </div>
                                </td>
							</tr>
						
							<tr>

								<td>Satu Mare</td>
								<td><a href="/panel/countries/edit/3/">Румыния</a></td>
                                <td>
                                    <div class="btn-group pull-right">
                                        <a href="/panel/states/edit/89" class="glyphicon glyphicon-pencil btn btn-default btn-sm"></a>
                                        <a href="/panel/cities/?filter=filter&amp;state=89" class="glyphicon glyphicon-eye-open btn btn-default btn-sm"></a>

                                    </div>
                                </td>
							</tr>
						
						</tbody>
					</table>

                    


<ul class="pagination">

  <li class="disabled"><a href="#" onclick="javascript: return false;">«</a></li>


  
    
  <li class="active"><a href="#" onclick="javascript: return false;">1</a></li>
    
  

  
    
  <li><a href="?page=2">2</a></li>
    
  

  
    
  <li><a href="?page=3">3</a></li>
    
  

  
    
  <li><a href="?page=4">4</a></li>
    
  

  
    
  <li><a href="?page=5">5</a></li>
    
  


  <li><a href="?page=2">»</a></li>

</ul>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
<!-- /.content -->
