<section class="content-header">
    <h1>
	    Simcards
        <small>Simcards</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Simcards</li>
    </ol>
</section>

<form>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-header">
                    <a href="/panel/simcards/create/" class="btn btn-sm btn-primary">
                        <span class="glyphicon glyphicon-new-window"></span> <b>Add item </b>
                    </a>
                </div>
                <div class="box-body table-responsive no-padding">
					<table class="table table-hover table-vcenter">
						<thead>
							<tr role="row">
								<th><select class="form-control select2 select2-hidden-accessible" id="id_country" name="country" tabindex="-1" aria-hidden="true">
<option value="" selected="selected">---------</option>
<option value="3">Румыния</option>
</select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 119px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-id_country-container"><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></th>
								<th><input class="form-control" id="id_phone" maxlength="512" name="phone" type="text"></th>
								<th><input class="form-control" id="id_inner_number" name="inner_number" type="text"></th>
								<th><input class="form-control" id="id_count" name="count" type="text"></th>
								<th><div class="icheckbox_minimal-green" aria-checked="false" aria-disabled="false" style="position: relative;"><input id="id_empty" name="empty" type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins></div></th>
								<th><input class="form-control" id="id_port" maxlength="512" name="port" type="text"></th>
                                <th style="width: 116px; min-width:116px;">
                                    <button name="filter" value="filter" type="submit" class="btn btn-block btn-success">
                                        <span class="glyphicon glyphicon-filter"></span> <b>Filtru</b>
                                    </button>
                                </th>
							</tr>
							<tr role="row">
								<th>Tară</th>
                                <th>Telefon</th>
								<th>Inner number</th>
								<th>Suma</th>
								<th>Empty</th>
								<th>Port</th>
                                <th style="width: 116px; min-width:116px;">
                                </th>
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>

                </div>
                <div class="box-footer">
                    
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
</form>
<!-- /.content -->
