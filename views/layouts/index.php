<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
if(isset($_SESSION['name']))
{
    if($_SESSION['name']!='admin')
    {
        header("Location:login.php?id=Вы не авторизированы. Для доступа к этой странице, введите логин и пароль.");
    }
}
else
{
    header("Location:login.php?id=Вы не авторизированы. Для доступа к этой странице, введите логин и пароль.");
} 
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />

    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="dist/lib_sweetalert/sweet-alert.css">

   <link href="dist/datepicker/datepicker.css" rel="stylesheet" type="text/css" />

  </head>
  <body class="skin-blue">
    <!-- Site wrapper -->
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="http://admin-crm.com/panel/dashboard/" class="logo">

            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Admin</b> - crm</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="http://admin-crm.com/panel/clients/#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">

                <div class="pull-left" style="margin-top: 10px;">
                     <form class="pull-right" action="http://admin-crm.com/i18n/setlang/" method="POST">
                        
                        
                            <input class="btn btn-sm btn-primary" name="language" type="submit" value="en">
                        
                            <input class="btn btn-sm btn-primary" name="language" type="submit" value="ro">
                        
                            <input class="btn btn-sm btn-primary" name="language" type="submit" value="ru">
                        
                    </form>
                </div>
                <ul class="nav navbar-nav">
                    
                    <li class="dropdown user user-menu">
                        <a href="http://admin-crm.com/panel/clients/#">
                            <span class="fa fa-user fa-lg"></span> <span class="hidden-xs"><strong>Dumitru</strong></span>
                        </a>
                    </li>
                    <li>
                        <a href="http://admin-crm.com/panel/logout/">
                            <span class="fa fa-sign-out fa-lg"></span> <b>Deautentificare </b>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

      <!-- =============================================== -->
      <?php

     include($_SERVER['DOCUMENT_ROOT'].'/views/pages/menu.php');
     ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        

        <!-- Main content -->
        <section class="content">

          <?php
          include($_SERVER['DOCUMENT_ROOT'].'/views/pages/'.$alias.'.php');
          ?>

        </section>
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        
 
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->
    <script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    
    <!-- FastClick -->
    <script src='plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js" type="text/javascript"></script>
    <script src="plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="dist/lib_sweetalert/sweet-alert.min.js"></script>
    



  </body>
</html>